# Changelog

## [UNRELEASED]

## [1.0.7+10007] [2020-08-22]
### Added
- support for 'de_DE'-Locale

### Changed
- improved diagnostics
- smaller bugfixes

## [1.0.6+6] [2020-06-13]
### Added
- support for dark themes

## [1.0.5+5] [2020-06-12]
### Added
- support for "Early End"-Rounds

### Changed
- restyled the summary cards on the "Current Game"-Page
- skipped versionCode '4' that the patch version concurs with the versionCode 

## [1.0.3+4] [2020-06-09] [HOTFIX]
### Fixed
- instant crashes on start on android

## [1.0.2+3] [2020-06-08] [YANKED]
### Added
- license dialog
- support for "Thousand"-Rounds
- "What's new"-Page, which shows after updates

## [1.0.1+2] [2020-06-01]
### Added
- reference to code in the "About"-Page
- confirmation modal on "End Game"-Button

### Changed
- made code public

### Fixed
- centering on the "Create a Game"-Page if no teams are available
- loading of saved games

## [1.0.0+1] [2020-05-28]
### Added
- initial version
