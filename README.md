# Binokel Counter

This project is mainly for the purpose of learning flutter, and the different concepts that can be used for its development.
The app is basically a counter utility for your Binokel Games and is available on the PlayStore, [here](https://play.google.com/store/apps/details?id=de.personal.binokel_counter).

## Issues

If you encounter any problems or have requests/ideas for features, feel free to head over to the issues of the project and dropping me a message there.

## Development

If you want to help develop this app you will need to generate some code. The app is using [built_value](https://github.com/google/built_value.dart) for its model classes. Therefore you will have to start the `build_runner` once. You also can find these calls in the [.gitlab-ci.yml](.gitlab-ci.yml) file, as the CI does the same here.

To run the once:
```shell script
$ flutter pub run build_runner build
```

If you want to make some changes to the model you probably want to start the `build_runner` in `watch`-mode for automatic redeploy:
```shell script
$ flutter pub run build_runner watch
```

If you already had generated files there it might be necessary to add the option `--delete-conflicting-outputs` to override them.

## Release

The release process is now fully automated and should be triggered only from the CI-Pipelines. Therefore, consider the [Manual](#Manual) to be obsolete and only documented for informational purposes.

### Automatic

The Gitlab-CI pipelines are configured with the needed data to do a release from the `master`-Branch via the normal pipeline. The Pipeline-Stage itself is the `release`-stage which is only available on the `master`-Branch. It has to be triggered manually. If you want to change the version use the [`update_version`-script](bin/update_version.dart). 

Concluded the release process would be:
* update version, for different version increases you can use [`--patch`, `--minor`, `--major`]
```shell script
flutter pub run bin/update_version.dart --patch
```
* create the prepared merge request
* merge the merge request
* when the pipelines on the new master ran successfully you have the option to trigger the release stage, which uploads the release to the internal track, promotes this release to the alpha track and this in turn to the beta track immediately

### Manual

! ATTENTION ! 

This should not be necessary anymore and should only be considered when the automatic release process described in the [Automatic](#Automatic) section is not available or viable.

! ATTENTION !

This section describes basically what the CI pipeline, described in [Automatic](#Automatic), internally does.

For the release on Android you have to create your `key.properties` file in the [android](android)-Folder and configure the information needed for signing the app. After you have done that, you can build your signed `aab`-File via:
```shell script
flutter build appbundle --release
```

To create the changelogs which will automatically be uploaded to the PlayStore via `fastlane` you can run the [`changelog.dart`-tool](bin/changelog.dart), which creates the changelogs out of the [CHANGELOG.md](CHANGELOG.md) in the format `fastlane` expects.

```shell script
$ flutter pub run bin/changelog.md
```

After that you can deploy the application via `fastlane`. For this you need the json-key file to give `fastlane` access to the google developer console.

To deploy onto the `internal`-Track:
```shell script
$ JSON_KEY_DATA="$(cat json_key_file.json)" bundle exec fastlane upload_to_play_store_internal
```

To promote the `internal` release to the `alpha`-Track:
```shell script
$ JSON_KEY_DATA="$(cat json_key_file.json)" bundle exec fastlane promote_internal_to_alpha
```

To promote the `alpha` release to the `beta`-Track:
```shell script
$ JSON_KEY_DATA="$(cat json_key_file.json)" bundle exec fastlane promote_alpha_to_beta
```

## TODO

* `settings.gradle` needed to be changed, PR for flutter is underway, probably needs to be replaced in the future
