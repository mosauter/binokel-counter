import 'dart:io';

import 'package:args/args.dart';

Future<void> main(List<String> arguments) async {
  final parser = ArgParser()
    ..addFlag('override', defaultsTo: false)
    ..addOption('changelog', defaultsTo: 'CHANGELOG.md');

  final results = parser.parse(arguments);
  if (results.rest.isNotEmpty) {
    throw ArgumentError('Arguments: "${results.rest}" not recognized');
  }

  final changelogFile = File(results['changelog']);

  if (!changelogFile.existsSync()) {
    throw ArgumentError(
        'Changelog at: "${results['changelog']}" does not exist');
  }

  final regex = RegExp(r'^## \[(\d+.\d+.\d+\+\d+)]');
  final changelogLines = await changelogFile.readAsLines();

  final versionBlocks = <String, String>{};
  String currentVersion;
  for (final line in changelogLines) {
    final matches = regex.allMatches(line).toList();

    if (matches.isNotEmpty) {
      currentVersion = matches[0].group(1);
      versionBlocks[currentVersion] = line;
    } else if (currentVersion != null) {
      versionBlocks[currentVersion] = '${versionBlocks[currentVersion]}\n$line';
    }
  }

  await Future.wait(versionBlocks.keys.toList().reversed.map((version) {
    final versionCode = version.split('+')[1];
    return writeChangelog(versionCode, versionBlocks[version],
        override: results['override'], version: version);
  }));
}

Future<void> writeChangelog(String versionCode, String changelog,
    {bool override = false, String version}) async {
  final path =
      'android/fastlane/metadata/android/en-US/changelogs/$versionCode.txt';

  final file = File(path);

  if (!override && file.existsSync()) {
    throw StateError('changelog at: $path, already exists');
  }

  await file.create(recursive: true);

  print('Creating "$path" for version: [$version]');
  return file.writeAsString(changelog);
}

// ignore_for_file: avoid_print
