import 'dart:io';

import 'package:args/args.dart';
import 'package:meta/meta.dart';
import 'package:open_url/open_url.dart';

class GitCommands {
  GitCommands({@required this.dryRun});

  static const mergeRequestLink =
      'https://gitlab.com/mosauter/binokel-counter/-/merge_requests/new?merge_request%5Bsource_branch%5D=';

  final bool dryRun;

  void checkMaster() {
    final String branch =
        Process.runSync('git', ['rev-parse', '--abbrev-ref', 'HEAD'])
            .stdout
            .replaceAll('\n', '');
    print('We are on branch: $branch');

    _updateAssert(branch == 'master', 'Releases can only be made from master',
        dryRun: dryRun);
  }

  String checkoutReleaseBranch(String version) {
    final releaseBranch = 'release-$version';
    if (dryRun) {
      print('Dry Run, therefore not checking out $releaseBranch');
      return null;
    }

    final checkoutResult =
        Process.runSync('git', ['checkout', '-b', releaseBranch]);

    _updateAssert(checkoutResult.exitCode == 0,
        'Checkout to release branch did not work');

    return releaseBranch;
  }

  void commitChanges(String pubspec, String changelog, String version) {
    if (dryRun) {
      print('dryRun, therefore no commit and push here');
      return;
    }
    Process.runSync('git', ['add', pubspec, changelog]);
    Process.runSync('git', ['commit', '-m', '"[skip-ci] Release $version"']);
    Process.runSync(
        'git', ['push', '--set-upstream', 'origin', 'release-$version']);
  }
}

Future<void> main(List<String> arguments) async {
  final parser = ArgParser()
    ..addFlag('patch', defaultsTo: true)
    ..addFlag('minor', defaultsTo: false)
    ..addFlag('major', defaultsTo: false)
    ..addFlag('dryRun', defaultsTo: false)
    ..addOption('pubspec', defaultsTo: 'pubspec.yaml')
    ..addOption('changelog', defaultsTo: 'CHANGELOG.md');
  final args = parser.parse(arguments);
  final git = GitCommands(dryRun: args['dryRun'])..checkMaster();

  final pubspecFile = File(args['pubspec']);

  if (!pubspecFile.existsSync()) {
    throw ArgumentError('pubspec at: "${args['pubspec']}" does not exist');
  }

  final pubspec = await pubspecFile.readAsString();

  final regex = RegExp(r'version: (\d+).(\d+).(\d+)\+(\d+)');
  final result = regex.allMatches(pubspec);

  _updateAssert(result.length == 1,
      '''Multiple matches (${result.length}) for the regex, for security we abort here.''');

  final version = result.toList()[0];

  final major = int.parse(version.group(1));
  final minor = int.parse(version.group(2));
  final patch = int.parse(version.group(3));
  final versionCode = int.parse(version.group(4));

  final newVersion = _increaseVersion(major, minor, patch, args);

  final newVersionCode = _toVersionCode(newVersion);

  _updateAssert(versionCode < int.parse(newVersionCode),
      'New Version code should be higher than the old version code');

  final newVersionString = 'version: ${newVersion.join('.')}+$newVersionCode';

  print('''
Old ${version.group(0)}
New $newVersionString''');

  final releaseBranch =
      git.checkoutReleaseBranch('${newVersion.join('.')}+$newVersionCode');

  final newPubspec = pubspec.replaceFirst(version.group(0), newVersionString);

  final writeFile =
      args['dryRun'] ? File('dry-run.${args['pubspec']}') : pubspecFile;
  await writeFile.writeAsString(newPubspec);

  await _updatingChangelog(
      args['changelog'], '${newVersion.join('.')}+$newVersionCode',
      dryRun: args['dryRun']);

  git.commitChanges(args['pubspec'], args['changelog'],
      '${newVersion.join('.')}+$newVersionCode');

  await openUrl('${GitCommands.mergeRequestLink}$releaseBranch');
}

String _toVersionCode(List<int> version) =>
    '${version[0]}${version.skip(1).map(_toPaddedCode).join()}';

String _toPaddedCode(version) => version.toString().padLeft(2, '0');

List<int> _increaseVersion(
  int major,
  int minor,
  int patch,
  ArgResults args,
) {
  if (args['major']) {
    return [major + 1, 0, 0];
  } else if (args['minor']) {
    return [major, minor + 1, 0];
  } else if (args['patch']) {
    return [major, minor, patch + 1];
  } else {
    return [major, minor, patch];
  }
}

void _updateAssert(bool result, String message, {bool dryRun = false}) {
  if (!result) {
    if (dryRun) {
      print(message);
      print('Because it is a dryRun we do not abort here');
    } else {
      throw AssertionError(message);
    }
  }
}

const _changelogHeader = '''
# Changelog

## [UNRELEASED]''';

Future<void> _updatingChangelog(String changelogPath, String version,
    {bool dryRun = false}) async {
  print('Updating changelog');

  final changelogFile = File(changelogPath);

  _updateAssert(changelogFile.existsSync(),
      'Changelog at: "$changelogPath" does not exist');

  final changelog = await changelogFile.readAsString();
  final currentDate = DateTime.now().toIso8601String().split('T')[0];
  final updatedChangelog = changelog
      .replaceFirst('[UNRELEASED]', '[$version] [$currentDate]')
      .replaceFirst('# Changelog', _changelogHeader);

  final writeFile = dryRun ? File('dry-run.$changelogPath') : changelogFile;
  await writeFile.writeAsString(updatedChangelog);
}

// ignore_for_file: avoid_print
