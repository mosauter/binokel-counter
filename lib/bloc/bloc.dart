// For this case we explicitly want a function on the type and want to force
// the sub classes to implement it. Therefore we need an abstract class
// with 'only' one member here.
// ignore: one_member_abstracts
abstract class Bloc {
  void dispose();
}
