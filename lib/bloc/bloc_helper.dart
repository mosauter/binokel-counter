import 'package:binokel_counter/bloc/bloc.dart';
import 'package:binokel_counter/widgets/bloc/bloc_provider_widget.dart';
import 'package:flutter/widgets.dart';

class BlocNotConfiguredError extends AssertionError {
  BlocNotConfiguredError() : super(_message);

  static const String _message =
      "bloc is not configured yet, you have to call 'configure' first";
}

class _BlocCapsule<T> {
  T bloc;
}

mixin BlocHelper<T extends Bloc> {
  final _BlocCapsule<T> _capsule = _BlocCapsule();

  T get bloc {
    if (_capsule.bloc == null) {
      throw BlocNotConfiguredError();
    }
    return _capsule.bloc;
  }

  void configure(BuildContext context) {
    _capsule.bloc = BlocProvider.of<T>(context);
  }
}
