import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class GameEvent {}

// Games Events

class GamesRefresh extends GameEvent {}

// Game Events

class StartGame extends GameEvent {
  StartGame(this.game);

  final BinokelGame game;
}

class EndGame extends GameEvent {}

class GameRefresh extends GameEvent {}

// Team Events

class AddTeam extends GameEvent {
  AddTeam(this.teamData);

  final BinokelTeam teamData;
}

class TeamRefresh extends GameEvent {}

// Round Events

class StartRound extends GameEvent {
  StartRound(this.round);

  final Round round;
}

class ReportRound extends GameEvent {
  ReportRound(this.round);

  final Round round;
}

class EndRound extends GameEvent {
  EndRound(this.round);

  final Round round;
}

class RoundRefresh extends GameEvent {}
