import 'dart:async';
import 'dart:collection';

import 'package:binokel_counter/bloc/bloc.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/model/persistence/saver.dart';
import 'package:logger/logger.dart';

/// Logger
final Logger l = Logger(printer: PrettyPrinter());

class BinokelBloc implements Bloc {
  BinokelBloc() {
    _load().then((_) {
      _teamController.sink.add(UnmodifiableListView(_availableTeams));
      _gamesController.sink.add(UnmodifiableListView(_games));
      _gameController.sink.add(_currentGame);
    });
    _eventController.stream.listen(_handleEvent);
  }

  final BinokelStorage _storage = BinokelStorage();
  final _availableTeams = <BinokelTeam>{};
  final _games = <BinokelGame>[];

  final _gamesController = StreamController<List<BinokelGame>>.broadcast();
  final _gameController = StreamController<BinokelGame>.broadcast();
  final _roundController = StreamController<Round>.broadcast();
  final _teamController = StreamController<List<BinokelTeam>>.broadcast();

  final _eventController = StreamController<GameEvent>();

  BinokelGame _currentGame;
  Round _currentRound;
  bool gameStarted = false;
  bool roundStarted = false;

  List<BinokelGame> get _gamesWithCurrentGame =>
      gameStarted ? [..._games, _currentGame] : _games;

  BinokelGame get _currentGameWithCurrentRound => roundStarted
      ? _currentGame.rebuild((game) => game.rounds..add(_currentRound))
      : _currentGame;

  Stream<List<BinokelGame>> get gamesStream => _gamesController.stream;

  Stream<BinokelGame> get gameStream => _gameController.stream;

  Sink<BinokelGame> get _gameSink => _gameController.sink;

  Stream<Round> get roundStream => _roundController.stream;

  Sink<Round> get _roundSink => _roundController.sink;

  Stream<List<BinokelTeam>> get teamStream => _teamController.stream;

  Sink<List<BinokelTeam>> get _teamSink => _teamController.sink;

  Sink<GameEvent> get blocSink => _eventController.sink;

  Future<void> _load() async {
    _availableTeams.addAll(await _storage.loadTeams());
    _games.addAll(await _storage.loadGames());
    _currentGame = _games.isEmpty ? null : _games[0];
  }

  void _updateStreams() {
    _gamesController.sink.add(UnmodifiableListView(_gamesWithCurrentGame));
    _teamSink.add(UnmodifiableListView(_availableTeams));
    _gameSink.add(_currentGameWithCurrentRound);
    _roundSink.add(_currentRound);
  }

  Future<void> _save() async {
    await Future.wait([
      _storage.writeTeams(_availableTeams.toList()),
      _storage.writeGames(_games),
    ]);
  }

  void _handleEvent(GameEvent event) {
    l.d('[_handleEvent] type: ${event.runtimeType} | event: $event');
    switch (event.runtimeType) {
      // refresh
      case GamesRefresh:
        _gamesController.sink.add(UnmodifiableListView(_games));
        // single refresh, therefore early return
        return;
      case TeamRefresh:
        _teamSink.add(UnmodifiableListView(_availableTeams));
        // single refresh, therefore early return
        return;
      case GameRefresh:
        _gameSink.add(_currentGameWithCurrentRound);
        // single refresh, therefore early return
        return;
      case RoundRefresh:
        _roundSink.add(_currentRound);
        // single refresh, therefore early return
        return;
      // Team
      case AddTeam:
        final AddTeam addTeamEvent = event;
        _availableTeams.add(addTeamEvent.teamData);
        break;
      // Game
      case StartGame:
        final StartGame startEvent = event;
        _currentGame = startEvent.game;
        gameStarted = true;
        break;
      case EndGame:
        _games.add(_currentGame);
        _currentGame = null;
        gameStarted = false;
        break;
      // Round
      case StartRound:
        final StartRound startRoundEvent = event;
        _currentRound = startRoundEvent.round;
        roundStarted = true;
        break;
      case ReportRound:
        final ReportRound reportRoundEvent = event;
        _currentRound = reportRoundEvent.round;
        break;
      case EndRound:
        final EndRound endRoundEvent = event;
        _currentGame = _currentGame.addNewRound(endRoundEvent.round);
        _currentRound = null;
        roundStarted = false;
        break;
      default:
        throw UnimplementedError('GameEvent ${event.runtimeType} is not '
            'implemented yet');
    }
    _save();
    _updateStreams();
  }

  @override
  void dispose() {
    _gamesController.close();
    _gameController.close();
    _roundController.close();
    _teamController.close();
    _eventController.close();
  }
}
