import 'dart:async';

import 'package:binokel_counter/bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum BinokelThemeMode { light, dark, system }

const _binokelThemeModes = [
  BinokelThemeMode.system,
  BinokelThemeMode.light,
  BinokelThemeMode.dark,
];

class ThemeBloc extends Bloc {
  ThemeBloc() {
    _blocController.stream.listen(_handleChange);
    _loadFromPreferences();
  }

  static const _themePrefKey = 'binokel_theme_mode';

  BinokelThemeMode _currentMode = BinokelThemeMode.system;

  final _blocController = StreamController<BinokelThemeMode>();
  final _themeController =
      BehaviorSubject<BinokelThemeMode>.seeded(BinokelThemeMode.system);

  Stream<BinokelThemeMode> get theme => _themeController.stream;

  Sink<BinokelThemeMode> get sink => _blocController.sink;

  void _handleChange(BinokelThemeMode mode) {
    if (_currentMode != mode) {
      _currentMode = mode;
      _themeController.sink.add(_currentMode);
      _saveToPreferences();
    }
  }

  @override
  void dispose() {
    _themeController.close();
    _blocController.close();
  }

  Future<void> _loadFromPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final theme = _intToTheme(prefs.getInt(_themePrefKey) ?? 0);

    _handleChange(theme);
  }

  Future<void> _saveToPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    if (_currentMode != _intToTheme(prefs.getInt(_themePrefKey) ?? 0)) {
      await prefs.setInt(_themePrefKey, _themeToInt(_currentMode));
    }
  }

  static int _themeToInt(BinokelThemeMode mode) =>
      _binokelThemeModes.indexOf(mode);

  static BinokelThemeMode _intToTheme(int index) => _binokelThemeModes[index];
}
