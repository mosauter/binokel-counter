import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/bloc/theme_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/bloc/bloc_provider_widget.dart';
import 'package:binokel_counter/widgets/view/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(BinokelApp());
}

class BinokelApp extends StatefulWidget {
  @override
  _BinokelAppState createState() => _BinokelAppState();
}

class _BinokelAppState extends State<BinokelApp> {
  final _binokelBloc = BinokelBloc();
  final _themeBloc = ThemeBloc();

  ThemeData _getTheme(BinokelThemeMode mode) {
    switch (mode) {
      case BinokelThemeMode.light:
        return ThemeData(
          primarySwatch: AppConstants.mainColor,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        );
      case BinokelThemeMode.dark:
        return ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
          brightness: Brightness.dark,
        );
      default:
        throw ArgumentError('Mode: $mode | should not be in this branch');
    }
  }

  @override
  Widget build(BuildContext context) {
    final binokelRoutes = getRoutes();

    return BlocProvider(
        bloc: _themeBloc,
        child: BlocProvider(
            bloc: _binokelBloc,
            child: StreamBuilder<BinokelThemeMode>(
                stream: _themeBloc.theme,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return AppConstants.loadingAnimation;
                  }

                  final theme = snapshot.data;

                  if (theme == BinokelThemeMode.system) {
                    return MaterialApp(
                      localizationsDelegates: [
                        S.delegate,
                        GlobalMaterialLocalizations.delegate,
                        GlobalWidgetsLocalizations.delegate,
                        GlobalCupertinoLocalizations.delegate,
                      ],
                      supportedLocales: S.delegate.supportedLocales,
                      theme: _getTheme(BinokelThemeMode.light),
                      darkTheme: _getTheme(BinokelThemeMode.dark),
                      initialRoute: binokelRoutes.initialRoute,
                      routes: binokelRoutes.routes,
                    );
                  }

                  return MaterialApp(
                    localizationsDelegates: [
                      S.delegate,
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate,
                    ],
                    supportedLocales: S.delegate.supportedLocales,
                    theme: _getTheme(theme),
                    initialRoute: binokelRoutes.initialRoute,
                    routes: binokelRoutes.routes,
                  );
                })));
  }

  @override
  void dispose() {
    _binokelBloc.dispose();
    _themeBloc.dispose();
    super.dispose();
  }
}
