import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'game.g.dart';

abstract class BinokelGame implements Built<BinokelGame, BinokelGameBuilder> {
  factory BinokelGame([void Function(BinokelGameBuilder) updates]) =
      _$BinokelGame;

  BinokelGame._();

  BinokelTeam get teamOne;

  BinokelTeam get teamTwo;

  BuiltList<Round> get rounds;

  static Serializer<BinokelGame> get serializer => _$binokelGameSerializer;

  BinokelGame addNewRound(Round round) {
    final newRounds = rounds.toBuilder()..add(round);
    return rebuild((game) => game..rounds = newRounds);
  }
}
