import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:meta/meta.dart';

part 'early_end_round.g.dart';

abstract class EarlyEndRound
    with RoundType
    implements Round, Built<EarlyEndRound, EarlyEndRoundBuilder> {
  factory EarlyEndRound([void Function(EarlyEndRoundBuilder) updates]) =
      _$EarlyEndRound;

  EarlyEndRound._();

  @visibleForTesting
  static const bonusPointsForOtherTeam = 40;
  @visibleForTesting
  static const penaltyMultiplier = -1;

  static const roundType = 'early';

  @override
  String get roundTypeDescription => roundType;

  @override
  int get bid;

  @override
  bool get teamOneBid;

  @override
  @memoized
  bool get teamTwoBid => !teamOneBid;

  @override
  bool get done => true;

  /// The amount the team, who has not got the highest bid had to report.
  /// The other team did not want to play the round, therefore their report
  /// has no meaning.
  int get reported;

  @override
  int get resultTeamOne => _calcResult(true);

  @override
  int get resultTeamTwo => _calcResult(false);

  static Serializer<EarlyEndRound> get serializer => _$earlyEndRoundSerializer;

  int _calcResult(bool teamOne) {
    if (teamOne && teamOneBid || !teamOne && teamTwoBid) {
      return penaltyMultiplier * bid;
    }

    return reported + bonusPointsForOtherTeam;
  }
}
