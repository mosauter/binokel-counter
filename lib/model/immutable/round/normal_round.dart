import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:meta/meta.dart';

part 'normal_round.g.dart';

abstract class NormalRound
    with RoundType
    implements Round, Built<NormalRound, NormalRoundBuilder> {
  factory NormalRound([void Function(NormalRoundBuilder) updates]) =
      _$NormalRound;

  NormalRound._();

  static Serializer<NormalRound> get serializer => _$normalRoundSerializer;

  @visibleForTesting
  static const missedBidPenaltyMultiplier = -2;
  static const maxPointsPerRound = 250;

  static const roundType = 'normal';

  @override
  String get roundTypeDescription => roundType;

  @override
  int get bid;

  @override
  bool get teamOneBid;

  @override
  @memoized
  bool get teamTwoBid => !teamOneBid;

  @nullable
  int get reportedTeamOne;

  @nullable
  int get reportedTeamTwo;

  @nullable
  int get pointsTeamOne;

  @nullable
  int get pointsTeamTwo;

  @override
  @memoized
  int get resultTeamOne => _calcResult(teamOne: true);

  @override
  @memoized
  int get resultTeamTwo => _calcResult(teamOne: false);

  @override
  @memoized
  bool get done =>
      (pointsTeamOne ?? 0) + (pointsTeamTwo ?? 0) == maxPointsPerRound;

  int get _summedTeamOne => (pointsTeamOne ?? 0) + (reportedTeamOne ?? 0);

  int get _summedTeamTwo => (pointsTeamTwo ?? 0) + (reportedTeamTwo ?? 0);

  int _calcResult({@required bool teamOne}) {
    if (!done) {
      return 0;
    }

    if (teamOne) {
      // points for team one
      if (teamOneBid && _summedTeamOne < bid) {
        return missedBidPenaltyMultiplier * bid;
      }
      if (teamOneBid && _summedTeamOne >= bid) {
        return _summedTeamOne;
      }

      if (pointsTeamTwo == maxPointsPerRound) {
        // team one had no points, loose everything
        return 0;
      }
      return _summedTeamOne;
    }

    if (teamTwoBid && _summedTeamTwo < bid) {
      return missedBidPenaltyMultiplier * bid;
    }

    if (teamTwoBid && _summedTeamTwo >= bid) {
      return _summedTeamTwo;
    }

    if (pointsTeamOne == maxPointsPerRound) {
      // team two had no points, loose everything
      return 0;
    }
    return _summedTeamTwo;
  }

  NormalRound setPoints(int points, {bool teamOne}) {
    if (teamOne) {
      return rebuild((round) => round
        ..pointsTeamOne = points
        ..pointsTeamTwo = maxPointsPerRound - points);
    } else {
      return rebuild((round) => round
        ..pointsTeamOne = maxPointsPerRound - points
        ..pointsTeamTwo = points);
    }
  }
}
