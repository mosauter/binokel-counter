import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/round/early_end_round.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round.dart';
import 'package:flutter/cupertino.dart';

abstract class Round {
  int get bid;

  bool get teamOneBid;
  bool get teamTwoBid;

  bool get done;

  int get resultTeamOne;

  int get resultTeamTwo;

  String get roundTypeDescription;

  String localRoundType(Round round, BuildContext context);
}

mixin RoundType {
  String localRoundType(Round round, BuildContext context) {
    final locale = S.of(context);

    switch (round.roundTypeDescription) {
      case EarlyEndRound.roundType:
        return locale.earlyEnd;
      case NormalRound.roundType:
        return locale.normalRound;
      case ThousandRound.roundType:
        return locale.thousandRound;
      default:
        throw UnimplementedError(
            'RuntimeType: $runtimeType is not implemented yet');
    }
  }
}
