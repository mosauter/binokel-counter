import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round_result.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'thousand_round.g.dart';

abstract class ThousandRound
    with RoundType
    implements Round, Built<ThousandRound, ThousandRoundBuilder> {
  factory ThousandRound([void Function(ThousandRoundBuilder) updates]) =
      _$ThousandRound;

  ThousandRound._();

  static Serializer<ThousandRound> get serializer => _$thousandRoundSerializer;

  static const _failedPenaltyMultiplier = -1;
  static const _normal = 1000;
  static const _laidDown = 2000;

  static const roundType = 'thousand';

  @override
  String get roundTypeDescription => roundType;

  @override
  int get bid;

  @override
  bool get teamOneBid;

  @override
  @memoized
  bool get teamTwoBid => !teamOneBid;

  @override
  bool get done => roundResult != null;

  @override
  int get resultTeamOne => _calcResult(true);

  @override
  int get resultTeamTwo => _calcResult(false);

  @nullable
  ThousandRoundResultEnum get roundResult;

  int _calcResult(bool teamOne) {
    if (!done) {
      return 0;
    }

    if (teamOne && teamOneBid || !teamOne && teamTwoBid) {
      switch (roundResult) {
        case ThousandRoundResultEnum.laidDown:
          return _laidDown;
        case ThousandRoundResultEnum.successful:
          return _normal;
        case ThousandRoundResultEnum.failed:
          return _failedPenaltyMultiplier * _normal;
        default:
          throw UnimplementedError('roundResult: $roundResult, is an unknown'
              'result.');
      }
    }

    return 0;
  }

  ThousandRound setResult(ThousandRoundResultEnum result) =>
      rebuild((b) => b..roundResult = result);
}
