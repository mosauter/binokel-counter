import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'thousand_round_result.g.dart';

class ThousandRoundResultEnum extends EnumClass {
  const ThousandRoundResultEnum._(String name) : super(name);

  static Serializer<ThousandRoundResultEnum> get serializer =>
      _$thousandRoundResultEnumSerializer;

  static const ThousandRoundResultEnum successful = _$successful;
  static const ThousandRoundResultEnum failed = _$failed;
  static const ThousandRoundResultEnum laidDown = _$laidDown;

  static BuiltSet<ThousandRoundResultEnum> get values => _$values;

  static ThousandRoundResultEnum valueOf(String name) => _$valueOf(name);
}
