import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'team.g.dart';

abstract class BinokelTeam implements Built<BinokelTeam, BinokelTeamBuilder> {
  factory BinokelTeam([void Function(BinokelTeamBuilder) updates]) =
      _$BinokelTeam;

  BinokelTeam._();

  String get teamName;

  String get teamMemberOne;

  String get teamMemberTwo;

  static Serializer<BinokelTeam> get serializer => _$binokelTeamSerializer;
}
