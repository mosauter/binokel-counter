import 'package:binokel_counter/model/immutable/game.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'persistent_games.g.dart';

abstract class PersistentGames
    implements Built<PersistentGames, PersistentGamesBuilder> {
  factory PersistentGames([void Function(PersistentGamesBuilder) updates]) =
      _$PersistentGames;

  PersistentGames._();

  BuiltList<BinokelGame> get games;

  static Serializer<PersistentGames> get serializer =>
      _$persistentGamesSerializer;
}
