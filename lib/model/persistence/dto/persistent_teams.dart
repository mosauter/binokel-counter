import 'package:binokel_counter/model/immutable/team.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'persistent_teams.g.dart';

abstract class PersistentTeams
    implements Built<PersistentTeams, PersistentTeamsBuilder> {
  factory PersistentTeams([void Function(PersistentTeamsBuilder) updates]) =
      _$PersistentTeams;

  PersistentTeams._();

  BuiltList<BinokelTeam> get teams;

  static Serializer<PersistentTeams> get serializer =>
      _$persistentTeamsSerializer;
}
