import 'dart:convert';
import 'dart:io';

import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/model/persistence/dto/persistent_games.dart';
import 'package:binokel_counter/model/persistence/dto/persistent_teams.dart';
import 'package:binokel_counter/model/serializers.dart';
import 'package:built_collection/built_collection.dart';
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';

/// Logger
final Logger l = Logger(printer: PrettyPrinter());

class BinokelStorage {
  /// Storage abstraction for [BinokelGame]s and [BinokelTeam]s.
  ///
  /// Generally the [BinokelGame]s are written to [gameFile] and the
  /// [BinokelTeam]s to [teamFile]. The parameter here is basically just there
  /// for testing as there are defaults available for both parameters. The
  /// default for [gameFile] is [_defaultGameFile] and for [teamFile] it's
  /// [_defaultTeamFile].
  ///
  /// Those defaults are located via [getApplicationDocumentsDirectory] and are
  /// named [_gameStoreFile] and [_teamStoreFile] in this directory.
  BinokelStorage({Future<File> gameFile, Future<File> teamFile})
      : _gameFile = gameFile ?? _defaultGameFile(),
        _teamFile = teamFile ?? _defaultTeamFile();

  static const _teamStoreFile = 'binokel_teams.json';
  static const _gameStoreFile = 'binokel_games.json';

  static Future<String> _documentPath() async =>
      (await getApplicationDocumentsDirectory()).path;

  static Future<File> _defaultGameFile() async =>
      File('${await _documentPath()}/$_gameStoreFile');

  static Future<File> _defaultTeamFile() async =>
      File('${await _documentPath()}/$_teamStoreFile');

  final Future<File> _gameFile;
  final Future<File> _teamFile;

  Future<File> writeTeams(List<BinokelTeam> teams) async {
    l.d('[writeTeams] teams: $teams');

    final persistentTeams =
        PersistentTeams((b) => b..teams = ListBuilder(teams));
    final storage = await _teamFile;
    final jsonTeams = jsonEncode(serializers.serialize(persistentTeams));

    return storage.writeAsString(jsonTeams);
  }

  Future<List<BinokelTeam>> loadTeams() async {
    l.d('[loadTeams]');

    try {
      final storage = await _teamFile;

      if (!storage.existsSync()) {
        l.w("[loadTeams] file doesn't exist, silent return empty array");
        return [];
      }

      final teams = await storage.readAsString();

      l.d('[loadTeams] teams: $teams');

      return deserialize<PersistentTeams>(jsonDecode(teams)).teams.asList();
    } on FileSystemException catch (e) {
      l.w('[loadTeams]', e);
      return [];
    }
  }

  Future<File> writeGames(List<BinokelGame> games) async {
    l.d('[writeGames] games: $games');

    final persistentGames =
        PersistentGames((b) => b..games = ListBuilder(games));
    final storage = await _gameFile;
    final jsonGames = jsonEncode(serializers.serialize(persistentGames));

    return storage.writeAsString(jsonGames);
  }

  Future<List<BinokelGame>> loadGames() async {
    l.d('[loadGames]');

    try {
      final storage = await _gameFile;

      if (!storage.existsSync()) {
        l.w("[loadGames] file doesn't exist, silent return empty array");
        return [];
      }

      final games = await storage.readAsString();

      l.d('[loadGames] games: $games');

      return deserialize<PersistentGames>(jsonDecode(games)).games.asList();
    } on FileSystemException catch (e) {
      l.w('[loadGames]', e);
      return [];
    }
  }
}
