import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/early_end_round.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round_result.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/model/persistence/dto/persistent_games.dart';
import 'package:binokel_counter/model/persistence/dto/persistent_teams.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

part 'serializers.g.dart';

@SerializersFor([
  BinokelGame,
  BinokelTeam,
  PersistentTeams,
  PersistentGames,
  Round,
  NormalRound,
  ThousandRound,
  EarlyEndRound,
  ThousandRoundResultEnum,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();

// We work with dynamic here, therefore no type annotation necessary
// ignore: type_annotate_public_apis
T deserialize<T>(value) =>
    serializers.deserializeWith(serializers.serializerForType(T), value);

// We work with dynamic here, therefore no type annotation necessary
// ignore: type_annotate_public_apis
BuiltList<T> deserializeListOf<T>(value) => BuiltList.from(
    value.map((val) => deserialize<T>(val)).toList(growable: false));
