import 'package:binokel_counter/generated/l10n.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatefulWidget {
  static const routeName = '/about';

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  static const _fontSize = 20.0;

  static const _link = TextStyle(fontSize: _fontSize, color: Colors.blue);
  TextStyle _mainText(BuildContext context) => TextStyle(
      fontSize: _fontSize, color: Theme.of(context).textTheme.bodyText2.color);

  final _icon8Recognizer = TapGestureRecognizer()
    ..onTap = () {
      launch('https://icons8.com');
    };

  final _binokelRecognizer = TapGestureRecognizer()
    ..onTap = () {
      launch('https://de.wikipedia.org/wiki/Binokel');
    };

  final _gitlabRecognizer = TapGestureRecognizer()
    ..onTap = () {
      launch('https://gitlab.com/mosauter/binokel-counter');
    };

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).aboutTitle),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: RichText(
            textAlign: TextAlign.start,
            text: TextSpan(
              children: [
                TextSpan(
                    text: S.of(context).aboutFirstPart,
                    style: _mainText(context)),
                TextSpan(
                    text: S.of(context).binokel,
                    style: _link,
                    recognizer: _binokelRecognizer),
                TextSpan(
                    text: S.of(context).aboutSecondPart,
                    style: _mainText(context)),
                TextSpan(
                  text: S.of(context).aboutGitlabLink,
                  style: _link,
                  recognizer: _gitlabRecognizer,
                ),
                TextSpan(
                  text: S.of(context).aboutIconsUsed,
                  style: _link,
                  recognizer: _icon8Recognizer,
                ),
              ],
            ),
          ),
        ),
      );

  @override
  void dispose() {
    _icon8Recognizer.dispose();
    _binokelRecognizer.dispose();
    _gitlabRecognizer.dispose();
    super.dispose();
  }
}
