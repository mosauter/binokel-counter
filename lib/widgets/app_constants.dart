import 'package:binokel_counter/generated/l10n.dart';
import 'package:flutter/material.dart';

abstract class AppConstants {
  static const Color mainColor = Colors.green;
  static const Color lightColor = Colors.lightGreenAccent;

  static const Widget loadingAnimation = Padding(
    padding: EdgeInsets.all(8),
    child: Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(AppConstants.lightColor),
      ),
    ),
  );

  static Widget emptyMessage(BuildContext context) => Padding(
      padding: const EdgeInsets.only(top: 16),
      child: Center(
          child: Text(
        S.of(context).emptyMessage,
        style: const TextStyle(color: Colors.grey),
      )));

  static const SizedBox spaceBetweenRows = SizedBox(height: 10);

  static const summaryPadding = EdgeInsets.only(left: 16, right: 16, bottom: 8);
}
