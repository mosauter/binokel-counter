import 'package:binokel_counter/bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class BlocProvider<T extends Bloc> extends StatefulWidget {
  const BlocProvider({@required this.child, @required this.bloc, Key key})
      : super(key: key);

  final Widget child;
  final T bloc;

  static T of<T extends Bloc>(BuildContext context) =>
      context.findAncestorWidgetOfExactType<BlocProvider<T>>().bloc;

  @override
  _BlocProviderState createState() => _BlocProviderState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<T>('bloc', bloc));
  }
}

class _BlocProviderState extends State<BlocProvider> {
  @override
  Widget build(BuildContext context) => widget.child;

  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }
}
