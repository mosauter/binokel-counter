import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/game/game_current.dart';
import 'package:binokel_counter/widgets/teams/team_create.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CreateGamePage extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/game/add';

  final _formKey = GlobalKey<FormState>();
  final _builder = BinokelGameBuilder();

  Widget _notEnoughTeams(context) => Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Text(S.of(context).createGameNotEnoughTeamError),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, CreateTeam.routeName);
              },
              child: Text(S.of(context).createGameCreateTeamButton),
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    configure(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).createGameTitle),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: StreamBuilder<List<BinokelTeam>>(
              stream: bloc.teamStream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  bloc.blocSink.add(TeamRefresh());
                  return AppConstants.loadingAnimation;
                }

                final teams = snapshot.data;

                if (teams.isEmpty || teams.length < 2) {
                  // need at least two teams to start a game
                  return _notEnoughTeams(context);
                }

                _builder
                  ..teamOne = teams[0].toBuilder()
                  ..teamTwo = teams[1].toBuilder();

                return Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      _MultipleTeamChooser(
                        builder: _builder,
                        teams: teams,
                      ),
                      RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            bloc.blocSink.add(StartGame(_builder.build()));
                            Navigator.pushReplacementNamed(
                                context, CurrentGamePage.routeName);
                          }
                        },
                        child: Text(S.of(context).submitButton),
                      )
                    ],
                  ),
                );
              }),
        ),
      ),
    );
  }
}

class _MultipleTeamChooser extends StatefulWidget {
  const _MultipleTeamChooser(
      {@required this.builder, @required this.teams, Key key})
      : super(key: key);

  final BinokelGameBuilder builder;
  final List<BinokelTeam> teams;

  @override
  _MultipleTeamChooserState createState() => _MultipleTeamChooserState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelGameBuilder>('builder', builder))
      ..add(IterableProperty<BinokelTeam>('teams', teams));
  }
}

class _MultipleTeamChooserState extends State<_MultipleTeamChooser> {
  BinokelTeam get firstTeam => widget.builder.teamOne.build();

  set firstTeam(BinokelTeam team) {
    widget.builder.teamOne = team.toBuilder();
  }

  BinokelTeam get secondTeam => widget.builder.teamTwo.build();

  set secondTeam(BinokelTeam team) {
    widget.builder.teamTwo = team.toBuilder();
  }

  String _validate(BinokelTeam team) =>
      team == null ? S.of(context).createGameChooseTeam : null;

  @override
  Widget build(BuildContext context) {
    final allDropdownItems = widget.teams
        .map((e) => DropdownMenuItem(
              value: e,
              child: Text(e.teamName),
            ))
        .toList(growable: false);

    final firstDropdownEntries =
        List<DropdownMenuItem<BinokelTeam>>.from(allDropdownItems)
          ..retainWhere((element) => element.value != secondTeam);
    final secondDropdownEntries =
        List<DropdownMenuItem<BinokelTeam>>.from(allDropdownItems)
          ..retainWhere((element) => element.value != firstTeam);

    return Column(
      children: [
        DropdownButtonFormField(
          items: firstDropdownEntries,
          onChanged: (team) {
            setState(() {
              firstTeam = team;
            });
          },
          value: firstTeam,
          onSaved: (team) => firstTeam = team,
          decoration: InputDecoration(
            icon: const Icon(Icons.group),
            labelText: S.of(context).createGameFirstTeam,
          ),
          validator: _validate,
        ),
        DropdownButtonFormField(
          items: secondDropdownEntries,
          onChanged: (team) {
            setState(() {
              secondTeam = team;
            });
          },
          value: secondTeam,
          onSaved: (team) => secondTeam = team,
          decoration: InputDecoration(
            icon: const Icon(Icons.group),
            labelText: S.of(context).createGameSecondTeam,
          ),
          validator: _validate,
        )
      ],
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('firstTeam', firstTeam))
      ..add(DiagnosticsProperty<BinokelTeam>('secondTeam', secondTeam));
  }
}
