import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/game/summary/game.dart';
import 'package:binokel_counter/widgets/game/summary/team.dart';
import 'package:binokel_counter/widgets/round/round_create.dart';
import 'package:binokel_counter/widgets/round/round_finish.dart';
import 'package:binokel_counter/widgets/round/round_report.dart';
import 'package:binokel_counter/widgets/round/summary/round.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

/// Logger
final Logger l = Logger(printer: PrettyPrinter());

enum _ConfirmationAction { cancel, accept }

class CurrentGamePage extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/game/current';

  Future<_ConfirmationAction> _confirmationDialog(BuildContext context) async =>
      showDialog<_ConfirmationAction>(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
                title: Text(S.of(context).currentGameEndAlertTitle),
                content: Text(S.of(context).currentGameEndAlertSubtitle),
                actions: [
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(_ConfirmationAction.cancel);
                    },
                    child: Text(S.of(context).currentGameEndAlertCancel),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(_ConfirmationAction.accept);
                    },
                    child: Text(S.of(context).currentGameEndAlertAccept),
                  )
                ],
              ));

  @override
  Widget build(BuildContext context) {
    configure(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).currentGameTitle),
      ),
      body: StreamBuilder<BinokelGame>(
          stream: bloc.gameStream,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              bloc.blocSink.add(GameRefresh());
              return AppConstants.loadingAnimation;
            }

            final game = snapshot.data;

            final landscape =
                MediaQuery.of(context).orientation == Orientation.landscape;

            return Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: landscape ? 0 : 3,
                    child: _CurrentGameHeader(
                      teamOne: game.teamOne,
                      teamTwo: game.teamTwo,
                    ),
                  ),
                  Expanded(
                    flex: landscape ? 0 : 3,
                    child: GameSummaryCard(
                      game: game,
                    ),
                  ),
                  Expanded(
                    flex: landscape ? 1 : 16,
                    child: _RoundOverview(
                      game: game,
                    ),
                  ),
                ],
              ),
            );
          }),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 32),
            child: FloatingActionButton(
              heroTag: 'endGame',
              onPressed: () {
                _confirmationDialog(context).then((userResponse) {
                  if (userResponse == _ConfirmationAction.accept) {
                    bloc.blocSink.add(EndGame());
                    Navigator.pop(context);
                  }
                });
              },
              backgroundColor: Colors.redAccent,
              child: const Icon(Icons.stop),
            ),
          ),
          FloatingActionButton(
            heroTag: 'startRound',
            onPressed: () {
              Navigator.pushNamed(context, CreateRoundPage.routeName);
            },
            child: const Icon(Icons.add),
          )
        ],
      ),
    );
  }
}

class _CurrentGameHeader extends StatelessWidget {
  const _CurrentGameHeader(
      {@required this.teamOne, @required this.teamTwo, Key key})
      : super(key: key);

  final BinokelTeam teamOne;
  final BinokelTeam teamTwo;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TeamSummary(
              team: teamOne,
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            TeamSummary(
              team: teamTwo,
              crossAxisAlignment: CrossAxisAlignment.end,
            ),
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('teamOne', teamOne))
      ..add(DiagnosticsProperty<BinokelTeam>('teamTwo', teamTwo));
  }
}

class _RoundOverview extends StatelessWidget {
  const _RoundOverview({
    @required this.game,
    Key key,
  }) : super(key: key);

  final BinokelGame game;

  @override
  Widget build(BuildContext context) => ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: game.rounds.length,
        itemBuilder: (context, index) {
          final reversedIndex = game.rounds.length - index - 1;
          return RoundSummaryCard(
            teamOne: game.teamOne,
            teamTwo: game.teamTwo,
            round: game.rounds[reversedIndex],
            roundNumber: reversedIndex + 1,
            onTap: () {
              if (game.rounds[reversedIndex] is! NormalRound) {
                l.e('[build] round type: '
                    '${game.rounds[reversedIndex].runtimeType}');
                throw UnimplementedError(
                    'Round type: ${game.rounds[reversedIndex].runtimeType} '
                    'is not implemented yet');
              }

              final NormalRound round = game.rounds[reversedIndex];

              if (round.done) {
                // round finished, do nothing
                return;
              }

              if (round.reportedTeamOne == null ||
                  round.reportedTeamTwo == null) {
                // not reported, change to report
                Navigator.pushNamed(context, ReportRoundPage.routeName);
                return;
              }

              // not finished & reported, change to finish
              Navigator.pushNamed(context, FinishRoundPage.routeName);
            },
          );
        },
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<BinokelGame>('game', game));
  }
}
