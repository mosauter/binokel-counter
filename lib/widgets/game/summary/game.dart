import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class GameSummaryCard extends StatelessWidget {
  const GameSummaryCard({@required this.game, Key key}) : super(key: key);

  final BinokelGame game;

  int get teamOneScore => _calcScore(teamOne: true);

  int get teamTwoScore => _calcScore(teamOne: false);

  int _calcScore({bool teamOne}) => game.rounds
      .map((e) => teamOne ? e.resultTeamOne : e.resultTeamTwo)
      .fold<int>(0, (prev, e) => prev + e);

  @override
  Widget build(BuildContext context) => Card(
        child: Center(
            child: Column(
          children: [
            ListTile(
              title: Text(S.of(context).gameSummaryScores),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(S.of(context).gameSummaryTeamScore(
                      game.teamOne.teamName, teamOneScore)),
                  Text(S.of(context).gameSummaryTeamScore(
                      game.teamTwo.teamName, teamTwoScore)),
                ],
              ),
            )
          ],
        )),
      );
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelGame>('game', game))
      ..add(IntProperty('teamOneScore', teamOneScore))
      ..add(IntProperty('teamTwoScore', teamTwoScore));
  }
}
