import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TeamSummary extends StatelessWidget {
  const TeamSummary({
    @required this.team,
    @required this.crossAxisAlignment,
    Key key,
  }) : super(key: key);

  static const _teamNameStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const _memberStyle =
      TextStyle(fontSize: 15, color: Colors.grey, fontStyle: FontStyle.italic);

  final BinokelTeam team;
  final CrossAxisAlignment crossAxisAlignment;

  @override
  Widget build(BuildContext context) => Expanded(
        flex: 1,
        child: Column(
          crossAxisAlignment: crossAxisAlignment,
          children: [
            Text(team.teamName, style: _teamNameStyle),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    S.of(context).teamMemberOne(team.teamMemberOne),
                    style: _memberStyle,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    S.of(context).teamMemberTwo(team.teamMemberTwo),
                    style: _memberStyle,
                  ),
                ),
              ],
            )
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('team', team))
      ..add(EnumProperty<CrossAxisAlignment>(
          'crossAxisAlignment', crossAxisAlignment));
  }
}
