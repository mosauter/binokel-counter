import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/widgets/about/about.dart';
import 'package:binokel_counter/widgets/game/game_create.dart';
import 'package:binokel_counter/widgets/game/game_current.dart';
import 'package:binokel_counter/widgets/round/round_create.dart';
import 'package:binokel_counter/widgets/round/round_finish.dart';
import 'package:binokel_counter/widgets/teams/team_overview.dart';
import 'package:binokel_counter/widgets/view/changelog.dart';
import 'package:binokel_counter/widgets/view/theme_chooser.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class PageChooser extends StatelessWidget with BlocHelper<BinokelBloc> {
  void _closeDrawer(context) {
    Navigator.pop(context);
  }

  ListTile _getCurrentOrNewGame(context) {
    if (bloc.gameStarted) {
      return ListTile(
          title: Text(S.of(context).currentGameTitle),
          onTap: () {
            _closeDrawer(context);
            Navigator.pushNamed(context, CurrentGamePage.routeName);
          });
    }
    return ListTile(
        title: Text(S.of(context).createGameTitle),
        onTap: () {
          _closeDrawer(context);
          Navigator.pushNamed(context, CreateGamePage.routeName);
        });
  }

  Widget _getCreateOrFinishRound(context) {
    if (!bloc.gameStarted) {
      return Container();
    }
    if (bloc.roundStarted) {
      return ListTile(
          title: Text(S.of(context).finishRoundTitle),
          onTap: () {
            _closeDrawer(context);
            Navigator.pushNamed(context, FinishRoundPage.routeName);
          });
    } else {
      return ListTile(
          title: Text(S.of(context).createRoundTitle),
          onTap: () {
            _closeDrawer(context);
            Navigator.pushNamed(context, CreateRoundPage.routeName);
          });
    }
  }

  Widget _footer(context) => Align(
        alignment: FractionalOffset.bottomCenter,
        child: Column(
          children: [
            const Divider(),
            ListTile(
              title: Text(S.of(context).showChangelog),
              onTap: () {
                _closeDrawer(context);
                Navigator.pushNamed(context, ChangelogPage.routeName);
              },
            ),
            ListTile(
                title: Text(S.of(context).aboutTitle),
                onTap: () {
                  _closeDrawer(context);
                  Navigator.pushNamed(context, AboutPage.routeName);
                }),
            ListTile(
              title: Text(S.of(context).drawerLicenses),
              onTap: () async {
                final packageInfo = await PackageInfo.fromPlatform();
                showAboutDialog(
                    context: context,
                    applicationIcon: const Image(
                      image: AssetImage('assets/icon/icons8-cards-30.png'),
                    ),
                    applicationVersion:
                        '${packageInfo.version}+${packageInfo.buildNumber}',
                    applicationLegalese: S.of(context).drawerIconsLegalese);
              },
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    configure(context);

    return Drawer(
      child: Column(children: [
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                decoration:
                    BoxDecoration(color: Theme.of(context).primaryColor),
                child: Stack(children: [
                  Positioned(
                    bottom: 12,
                    child: Text(
                      S.of(context).appTitle,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                    ),
                  )
                ]),
              ),
              ThemeChooser(),
              ListTile(
                  title: Text(S.of(context).teamOverviewTitle),
                  onTap: () {
                    _closeDrawer(context);
                    Navigator.pushNamed(context, TeamOverview.routeName);
                  }),
              _getCurrentOrNewGame(context),
              _getCreateOrFinishRound(context),
            ],
          ),
        ),
        _footer(context),
      ]),
    );
  }
}
