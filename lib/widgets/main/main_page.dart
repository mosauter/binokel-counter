import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/game/game_create.dart';
import 'package:binokel_counter/widgets/game/summary/game.dart';
import 'package:binokel_counter/widgets/main/main_drawer.dart';
import 'package:binokel_counter/widgets/view/changelog.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// Logger
final Logger l =
    Logger(printer: PrettyPrinter(errorMethodCount: 0, printTime: true));

class MainPage extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const routeName = '/';

  static const _invalidVersion = '0.0.0+0';
  static const _preferencesVersionKey = 'last_app_version';

  Future<void> _scheduleCheckVersion(BuildContext context) async {
    final list = await Future.wait(
        [SharedPreferences.getInstance(), PackageInfo.fromPlatform()]);

    final SharedPreferences preferences = list[0];
    final savedVersion =
        preferences.getString(_preferencesVersionKey) ?? _invalidVersion;

    final PackageInfo packageInfo = list[1];
    final currentVersion = '${packageInfo.version}+${packageInfo.buildNumber}';
    l.d('[_scheduleCheckVersion] savedVersion: $savedVersion '
        '| currentVersion: $currentVersion');

    if (savedVersion != currentVersion) {
      await preferences.setString(_preferencesVersionKey, currentVersion);
      await Navigator.pushNamed(context, ChangelogPage.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    _scheduleCheckVersion(context);
    configure(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).appTitle),
      ),
      body: StreamBuilder<List<BinokelGame>>(
          stream: bloc.gamesStream,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              bloc.blocSink.add(GamesRefresh());
              return AppConstants.loadingAnimation;
            }

            final games = snapshot.data;

            if (games.isEmpty) {
              return AppConstants.emptyMessage(context);
            }

            return ListView.builder(
              itemCount: games.length,
              itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    if (index == games.length - 1 && bloc.gameStarted) {
                      // click on last tile, assuming current game
                      Navigator.pushNamed(context, CreateGamePage.routeName);
                    }
                  },
                  child: GameSummaryCard(game: games[index])),
            );
          }),
      drawer: PageChooser(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, CreateGamePage.routeName);
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
