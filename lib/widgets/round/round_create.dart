import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/round/round_report.dart';
import 'package:binokel_counter/widgets/teams/helper/single_team_chooser.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CreateRoundPage extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/round/add';

  final _formKey = GlobalKey<FormState>();
  final _builder = NormalRoundBuilder()..teamOneBid = true;

  @override
  Widget build(BuildContext context) {
    configure(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).createRoundTitle),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: StreamBuilder<BinokelGame>(
              stream: bloc.gameStream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  bloc.blocSink.add(GameRefresh());
                  return AppConstants.loadingAnimation;
                }

                final game = snapshot.data;

                return Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                          icon: const Icon(Icons.attach_money),
                          labelText: S.of(context).createRoundHighestBid,
                        ),
                        onSaved: (bid) => _builder.bid = int.parse(bid),
                        validator: (bid) =>
                            bid.isEmpty ? S.of(context).submitButton : null,
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                        ],
                      ),
                      SingleTeamChooser(
                        teamChange: (value) => _builder.teamOneBid = value,
                        teamOneBid: () => _builder.teamOneBid,
                        teams: [game.teamOne, game.teamTwo],
                      ),
                      RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            bloc.blocSink.add(StartRound(_builder.build()));
                            Navigator.pushReplacementNamed(
                                context, ReportRoundPage.routeName);
                          }
                        },
                        child: Text(S.of(context).submitButton),
                      )
                    ],
                  ),
                );
              }),
        ),
      ),
    );
  }
}
