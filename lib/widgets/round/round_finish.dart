import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round_result.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/round/summary/bid.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class _EndRoundBuilder {
  NormalRoundBuilder round;

  EndRound build() => EndRound(round.build());
}

class FinishRoundPage extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/round/finish';

  final _formKey = GlobalKey<FormState>();
  final _builder = _EndRoundBuilder();

  @override
  Widget build(BuildContext context) {
    configure(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).finishRoundTitle),
      ),
      body: SingleChildScrollView(
        child: StreamBuilder<Round>(
          stream: bloc.roundStream,
          builder: (_, roundSnapshot) => StreamBuilder<BinokelGame>(
            stream: bloc.gameStream,
            builder: (_, gameSnapshot) {
              if (!roundSnapshot.hasData || !gameSnapshot.hasData) {
                bloc.blocSink.add(
                    !roundSnapshot.hasData ? RoundRefresh() : GameRefresh());
                return AppConstants.loadingAnimation;
              }

              if (roundSnapshot.data is! NormalRound) {
                return ThousandRoundFinish(
                  round: roundSnapshot.data,
                  game: gameSnapshot.data,
                  bloc: bloc,
                );
              }

              final NormalRound round = roundSnapshot.data;
              final game = gameSnapshot.data;

              final highestBidTeam =
                  round.teamOneBid ? game.teamOne : game.teamTwo;

              _builder.round = round.toBuilder();
              return Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(children: [
                    RoundBidSummary(
                      highestBid: round.bid,
                      highestBidder: highestBidTeam.teamName,
                    ),
                    AppConstants.spaceBetweenRows,
                    _FinisherForm(
                      builder: _builder,
                      game: game,
                    ),
                    RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          bloc.blocSink.add(_builder.build());
                          Navigator.pop(context);
                        }
                      },
                      child: Text(S.of(context).submitButton),
                    )
                  ]),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

class ThousandRoundFinish extends StatefulWidget {
  const ThousandRoundFinish(
      {@required this.round, @required this.game, @required this.bloc, Key key})
      : super(key: key);

  final ThousandRound round;
  final BinokelGame game;
  final BinokelBloc bloc;

  BinokelTeam get highestBidTeam =>
      round.teamOneBid ? game.teamOne : game.teamTwo;

  @override
  _ThousandRoundFinishState createState() => _ThousandRoundFinishState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<ThousandRound>('round', round))
      ..add(DiagnosticsProperty<BinokelGame>('game', game))
      ..add(DiagnosticsProperty<BinokelBloc>('bloc', bloc))
      ..add(DiagnosticsProperty<BinokelTeam>('highestBidTeam', highestBidTeam));
  }
}

class _ThousandRoundFinishState extends State<ThousandRoundFinish> {
  ThousandRoundResultEnum _roundResult = ThousandRoundResultEnum.successful;

  void _changeResult(ThousandRoundResultEnum value) =>
      setState(() => _roundResult = value);

  ThousandRound _buildRound() =>
      widget.round.rebuild((b) => b.roundResult = _roundResult);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            RoundBidSummary(
              highestBid: widget.round.bid,
              highestBidder: widget.highestBidTeam.teamName,
            ),
            const Divider(),
            ListTile(
              title: Text(S.of(context).successfulRound),
              leading: Radio(
                value: ThousandRoundResultEnum.successful,
                groupValue: _roundResult,
                onChanged: _changeResult,
              ),
            ),
            ListTile(
              title: Text(S.of(context).laidDownRound),
              leading: Radio(
                value: ThousandRoundResultEnum.laidDown,
                groupValue: _roundResult,
                onChanged: _changeResult,
              ),
            ),
            ListTile(
              title: Text(S.of(context).failedRound),
              leading: Radio(
                value: ThousandRoundResultEnum.failed,
                groupValue: _roundResult,
                onChanged: _changeResult,
              ),
            ),
            const Divider(),
            RaisedButton(
              onPressed: () {
                final thousandRound = _buildRound();
                widget.bloc.blocSink.add(EndRound(thousandRound));
                Navigator.pop(context);
              },
              child: Text(S.of(context).submitButton),
            )
          ],
        ),
      );
}

class _FinisherForm extends StatefulWidget {
  const _FinisherForm({@required this.builder, @required this.game, Key key})
      : super(key: key);

  final _EndRoundBuilder builder;
  final BinokelGame game;

  void setPoints(int points, {bool teamOne}) {
    builder.round =
        builder.round.build().setPoints(points, teamOne: teamOne).toBuilder();
  }

  @override
  __FinisherFormState createState() => __FinisherFormState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<_EndRoundBuilder>('builder', builder))
      ..add(DiagnosticsProperty<BinokelGame>('game', game));
  }
}

class __FinisherFormState extends State<_FinisherForm> {
  BinokelTeam get teamOne => widget.game.teamOne;

  BinokelTeam get teamTwo => widget.game.teamTwo;

  int get pointsTeamOne => widget.builder.round.pointsTeamOne;

  set pointsTeamOne(int points) => widget.setPoints(points, teamOne: true);

  int get pointsTeamTwo => widget.builder.round.pointsTeamTwo;

  set pointsTeamTwo(int points) => widget.setPoints(points, teamOne: false);

  final TextEditingController _firstTeamTextController =
      TextEditingController();
  final TextEditingController _secondTeamTextController =
      TextEditingController();

  void _refreshPoints({bool teamOne, bool both = false}) {
    if (both || teamOne) {
      _firstTeamTextController.text = pointsTeamOne.toString();
    }
    if (both || !teamOne) {
      _secondTeamTextController.text = pointsTeamTwo.toString();
    }
  }

  String _validate(String points) {
    if (points.isEmpty) {
      return S.of(context).finishRoundPointsEmpty;
    }
    final p = int.parse(points);
    if (p < 0) {
      return S.of(context).finishRoundPointsNegative;
    }
    if (p > NormalRound.maxPointsPerRound) {
      return S
          .of(context)
          .finishRoundPointsMaximum(NormalRound.maxPointsPerRound);
    }
    return null;
  }

  @override
  Widget build(BuildContext context) => Column(
        children: [
          TextFormField(
            controller: _firstTeamTextController,
            decoration: InputDecoration(
                icon: const Icon(Icons.attach_money),
                labelText:
                    S.of(context).finishRoundPointsOfTeam(teamOne.teamName)),
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: (points) {
              pointsTeamOne = int.tryParse(points) ?? 0;
              setState(() {
                _refreshPoints(teamOne: false);
              });
            },
            validator: _validate,
          ),
          TextFormField(
            controller: _secondTeamTextController,
            decoration: InputDecoration(
                icon: const Icon(Icons.attach_money),
                labelText:
                    S.of(context).finishRoundPointsOfTeam(teamTwo.teamName)),
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: (points) {
              pointsTeamTwo = int.tryParse(points) ?? 0;
              _refreshPoints(teamOne: true);
            },
            validator: _validate,
          )
        ],
      );

  @override
  void dispose() {
    _firstTeamTextController.dispose();
    _secondTeamTextController.dispose();
    super.dispose();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('teamOne', teamOne))
      ..add(DiagnosticsProperty<BinokelTeam>('teamTwo', teamTwo))
      ..add(IntProperty('pointsTeamOne', pointsTeamOne))
      ..add(IntProperty('pointsTeamTwo', pointsTeamTwo));
  }
}
