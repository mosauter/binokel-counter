import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/early_end_round.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/round/round_finish.dart';
import 'package:binokel_counter/widgets/round/summary/bid.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ReportRoundPage extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/round/report';

  @override
  Widget build(BuildContext context) {
    configure(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).reportRoundTitle),
      ),
      body: StreamBuilder<Round>(
          stream: bloc.roundStream,
          builder: (_, roundSnapshot) => StreamBuilder<BinokelGame>(
                stream: bloc.gameStream,
                builder: (_, gameSnapshot) {
                  if (!roundSnapshot.hasData || !gameSnapshot.hasData) {
                    bloc.blocSink.add(!roundSnapshot.hasData
                        ? RoundRefresh()
                        : GameRefresh());
                    return AppConstants.loadingAnimation;
                  }

                  return _ChooseReportRound(
                    round: roundSnapshot.data,
                    game: gameSnapshot.data,
                    bloc: bloc,
                  );
                },
              )),
    );
  }
}

class _ChooseReportRound extends StatefulWidget {
  const _ChooseReportRound({
    @required this.round,
    @required this.game,
    @required this.bloc,
  });

  final Round round;
  final BinokelGame game;
  final BinokelBloc bloc;

  BinokelTeam get highestBidTeam =>
      round.teamOneBid ? game.teamOne : game.teamTwo;

  @override
  __ChooseReportRoundState createState() => __ChooseReportRoundState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<Round>('round', round))
      ..add(DiagnosticsProperty<BinokelGame>('game', game))
      ..add(DiagnosticsProperty<BinokelBloc>('bloc', bloc))
      ..add(DiagnosticsProperty<BinokelTeam>('highestBidTeam', highestBidTeam));
  }
}

enum _RoundType { thousand, normal, earlyEnd }

class __ChooseReportRoundState extends State<_ChooseReportRound> {
  _RoundType _roundType = _RoundType.normal;

  static ThousandRound _asThousandRound(Round round) {
    if (round is ThousandRound) {
      return round;
    }
    return ThousandRound((b) => b
      ..bid = round.bid
      ..teamOneBid = round.teamOneBid);
  }

  static NormalRound _asNormalRound(Round round) {
    if (round is NormalRound) {
      return round;
    }
    return NormalRound((b) => b
      ..bid = round.bid
      ..teamOneBid = round.teamOneBid);
  }

  Widget _reportWidget() {
    switch (_roundType) {
      case _RoundType.thousand:
        return _ReportThousandRound(
          round: _asThousandRound(widget.round),
          bloc: widget.bloc,
        );
      case _RoundType.normal:
        return _ReportNormalRound(
            round: _asNormalRound(widget.round),
            game: widget.game,
            bloc: widget.bloc);
      case _RoundType.earlyEnd:
        return _ReportEarlyEndRound(
          round: widget.round,
          game: widget.game,
          bloc: widget.bloc,
        );

      default:
        throw UnimplementedError(
            'round: $_roundType, type is not implemented yet');
    }
  }

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            ListTile(
              title: Text(S.of(context).normalRound),
              leading: Radio(
                value: _RoundType.normal,
                groupValue: _roundType,
                onChanged: (value) => setState(() => _roundType = value),
              ),
            ),
            ListTile(
              title: Text(S.of(context).thousandRound),
              leading: Radio(
                value: _RoundType.thousand,
                groupValue: _roundType,
                onChanged: (value) => setState(() => _roundType = value),
              ),
            ),
            ListTile(
              title: Text(S.of(context).earlyEnd),
              leading: Radio(
                value: _RoundType.earlyEnd,
                groupValue: _roundType,
                onChanged: (value) => setState(() => _roundType = value),
              ),
            ),
            const Divider(),
            RoundBidSummary(
              highestBid: widget.round.bid,
              highestBidder: widget.highestBidTeam.teamName,
            ),
            const Divider(),
            _reportWidget(),
          ],
        ),
      );
}

class _ReportThousandRound extends StatelessWidget {
  _ReportThousandRound({@required this.round, @required this.bloc, Key key})
      : event = ReportRound(round),
        super(key: key);

  final ThousandRound round;
  final BinokelBloc bloc;

  final ReportRound event;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            RaisedButton(
              onPressed: () {
                bloc.blocSink.add(event);
                Navigator.pushReplacementNamed(
                    context, FinishRoundPage.routeName);
              },
              child: Text(S.of(context).submitButton),
            )
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<ThousandRound>('round', round))
      ..add(DiagnosticsProperty<BinokelBloc>('bloc', bloc))
      ..add(DiagnosticsProperty<ReportRound>('event', event));
  }
}

class _ReportEarlyEndRound extends StatelessWidget {
  _ReportEarlyEndRound(
      {@required this.game,
      @required Round round,
      @required this.bloc,
      Key key})
      : _builder = EarlyEndRoundBuilder()
          ..bid = round.bid
          ..teamOneBid = round.teamOneBid,
        super(key: key);

  final BinokelGame game;
  final BinokelBloc bloc;

  final EarlyEndRoundBuilder _builder;
  final _formKey = GlobalKey<FormState>();

  String get teamToReport =>
      _builder.teamOneBid ? game.teamTwo.teamName : game.teamOne.teamName;

  @override
  Widget build(BuildContext context) => Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(
                icon: const Icon(Icons.save),
                labelText: S.of(context).reportRoundEnterMessage(teamToReport),
              ),
              onSaved: (report) =>
                  _builder.reported = int.tryParse(report) ?? 0,
              keyboardType: TextInputType.number,
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
              ],
            ),
            RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();

                  bloc.blocSink.add(EndRound(_builder.build()));

                  Navigator.pop(context);
                }
              },
              child: Text(S.of(context).submitButton),
            ),
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelBloc>('bloc', bloc))
      ..add(DiagnosticsProperty<BinokelGame>('game', game))
      ..add(StringProperty('teamToReport', teamToReport));
  }
}

class _ReportNormalRoundBuilder {
  NormalRoundBuilder round;

  ReportRound build() => ReportRound(round.build());
}

class _ReportNormalRound extends StatelessWidget {
  _ReportNormalRound({
    @required this.round,
    @required this.game,
    @required this.bloc,
    Key key,
  })  : _builder = _ReportNormalRoundBuilder()..round = round.toBuilder(),
        super(key: key);

  final NormalRound round;
  final BinokelGame game;
  final BinokelBloc bloc;
  final _ReportNormalRoundBuilder _builder;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) => Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(
                  icon: const Icon(Icons.save),
                  labelText: S
                      .of(context)
                      .reportRoundEnterMessage(game.teamOne.teamName),
                ),
                onSaved: (bid) =>
                    _builder.round.reportedTeamOne = int.tryParse(bid) ?? 0,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
              ),
              TextFormField(
                decoration: InputDecoration(
                  icon: const Icon(Icons.save),
                  labelText: S
                      .of(context)
                      .reportRoundEnterMessage(game.teamTwo.teamName),
                ),
                onSaved: (bid) =>
                    _builder.round.reportedTeamTwo = int.tryParse(bid) ?? 0,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
              ),
              RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    bloc.blocSink.add(_builder.build());
                    Navigator.pushReplacementNamed(
                        context, FinishRoundPage.routeName);
                  }
                },
                child: Text(S.of(context).submitButton),
              )
            ],
          ),
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<NormalRound>('round', round))
      ..add(DiagnosticsProperty<BinokelGame>('game', game))
      ..add(DiagnosticsProperty<BinokelBloc>('bloc', bloc));
  }
}
