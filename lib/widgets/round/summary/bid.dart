import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class RoundBidSummary extends StatelessWidget {
  const RoundBidSummary(
      {@required this.highestBid, @required this.highestBidder, Key key})
      : super(key: key);

  static const TextStyle _textStyle = TextStyle(fontSize: 20);

  final int highestBid;
  final String highestBidder;

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S.of(context).roundBidSummaryHighestBid,
                style: _textStyle,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Text(
                  highestBid.toString(),
                  style: _textStyle,
                ),
              )
            ],
          ),
          AppConstants.spaceBetweenRows,
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(
              S.of(context).roundBidSummaryHighestBidBy,
              style: _textStyle,
            ),
            Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Text(
                  highestBidder,
                  style: _textStyle,
                ))
          ]),
        ],
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(StringProperty('highestBidder', highestBidder))
      ..add(IntProperty('highestBid', highestBid));
  }
}
