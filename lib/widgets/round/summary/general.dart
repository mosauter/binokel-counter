import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class GeneralSummary extends StatelessWidget {
  const GeneralSummary({
    @required this.roundNumber,
    @required this.round,
    @required this.highestBidder,
    this.leading,
    Key key,
  }) : super(key: key);

  final int roundNumber;
  final Round round;
  final BinokelTeam highestBidder;
  final Widget leading;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: leading,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  S.of(context).generalSummaryDescription(
                      roundNumber, round.localRoundType(round, context)),
                  style: Theme.of(context)
                      .textTheme
                      .subtitle1
                      .copyWith(fontWeight: FontWeight.w500),
                ),
                Text(S.of(context).generalSummaryHighestBid(round.bid)),
                Text(S
                    .of(context)
                    .generalSummaryHighestBidder(highestBidder.teamName)),
              ],
            )
          ],
        ),
      );
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('highestBidder', highestBidder))
      ..add(IntProperty('roundNumber', roundNumber))
      ..add(DiagnosticsProperty<Round>('round', round));
  }
}
