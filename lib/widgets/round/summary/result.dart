import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ResultSummary extends StatelessWidget {
  const ResultSummary({
    @required this.teamOne,
    @required this.round,
    @required this.teamTwo,
    Key key,
  }) : super(key: key);

  final BinokelTeam teamOne;
  final BinokelTeam teamTwo;
  final Round round;

  @override
  Widget build(BuildContext context) => Padding(
        padding: AppConstants.summaryPadding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).resultSummaryResults,
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(fontWeight: FontWeight.w400),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Text(S.of(context).resultSummaryTeamResult(
                      teamOne.teamName, round.resultTeamOne)),
                ),
                Expanded(
                  flex: 1,
                  child: Text(S.of(context).resultSummaryTeamResult(
                      teamTwo.teamName, round.resultTeamTwo)),
                )
              ],
            ),
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<Round>('round', round))
      ..add(DiagnosticsProperty<BinokelTeam>('teamTwo', teamTwo))
      ..add(DiagnosticsProperty<BinokelTeam>('teamOne', teamOne));
  }
}
