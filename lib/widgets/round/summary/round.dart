import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/round/summary/general.dart';
import 'package:binokel_counter/widgets/round/summary/report.dart';
import 'package:binokel_counter/widgets/round/summary/result.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class RoundSummaryCard extends StatelessWidget {
  const RoundSummaryCard(
      {@required this.teamOne,
      @required this.teamTwo,
      @required this.round,
      @required this.roundNumber,
      this.onTap,
      Key key})
      : super(key: key);

  final BinokelTeam teamOne;
  final BinokelTeam teamTwo;
  final Round round;
  final int roundNumber;
  final Function() onTap;

  bool get finished => round.done;

  Widget get _summaryCard => finished
      ? _FinishedRoundSummaryCard(
          roundNumber: roundNumber,
          round: round,
          teamOne: teamOne,
          teamTwo: teamTwo,
        )
      : _RunningRoundSummaryCard(
          roundNumber: roundNumber,
          round: round,
          teamOne: teamOne,
          teamTwo: teamTwo,
        );

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: onTap,
        child: _summaryCard,
      );
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('teamOne', teamOne))
      ..add(DiagnosticsProperty<BinokelTeam>('teamTwo', teamTwo))
      ..add(DiagnosticsProperty<Round>('round', round))
      ..add(IntProperty('roundNumber', roundNumber))
      ..add(DiagnosticsProperty<Function()>('onTap', onTap))
      ..add(DiagnosticsProperty<bool>('finished', finished));
  }
}

mixin _Reportable {
  Round get round;

  BinokelTeam get teamOne;

  BinokelTeam get teamTwo;

  Widget get report {
    if (round is NormalRound) {
      final NormalRound normalRound = round;
      if (normalRound.reportedTeamOne != null &&
          normalRound.reportedTeamTwo != null) {
        return ReportSummary(teamOne: teamOne, round: round, teamTwo: teamTwo);
      }
    }
    return Container();
  }
}

class _FinishedRoundSummaryCard extends StatelessWidget with _Reportable {
  _FinishedRoundSummaryCard(
      {@required this.teamOne,
      @required this.teamTwo,
      @required this.round,
      @required this.roundNumber,
      Key key})
      : super(key: key);

  static const _successColor = Colors.green;
  static const _failureColor = Colors.red;

  @override
  final BinokelTeam teamOne;
  @override
  final BinokelTeam teamTwo;
  @override
  final Round round;
  final int roundNumber;

  BinokelTeam get highestBidder => round.teamOneBid ? teamOne : teamTwo;

  Icon get _roundIcon => round.resultTeamOne >= 0 && round.resultTeamTwo >= 0
      ? const Icon(Icons.check, color: _successColor)
      : const Icon(Icons.close, color: _failureColor);

  @override
  Widget build(BuildContext context) => Card(
        child: Center(
          child: Column(
            children: [
              GeneralSummary(
                roundNumber: roundNumber,
                round: round,
                highestBidder: highestBidder,
                leading: _roundIcon,
              ),
              report,
              ResultSummary(teamOne: teamOne, round: round, teamTwo: teamTwo)
            ],
          ),
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(IntProperty('roundNumber', roundNumber))
      ..add(DiagnosticsProperty<BinokelTeam>('highestBidder', highestBidder));
  }
}

class _RunningRoundSummaryCard extends StatelessWidget with _Reportable {
  _RunningRoundSummaryCard(
      {@required this.teamOne,
      @required this.teamTwo,
      @required this.round,
      @required this.roundNumber,
      Key key})
      : super(key: key);

  @override
  final BinokelTeam teamOne;
  @override
  final BinokelTeam teamTwo;
  @override
  final Round round;
  final int roundNumber;

  BinokelTeam get highestBidder => round.teamOneBid ? teamOne : teamTwo;

  @override
  Widget build(BuildContext context) => Card(
        child: Center(
          child: Column(
            children: [
              GeneralSummary(
                roundNumber: roundNumber,
                round: round,
                highestBidder: highestBidder,
                leading: const SizedBox(
                    width: 22,
                    height: 22,
                    child: Center(
                        child: CircularProgressIndicator(
                      strokeWidth: 2,
                    ))),
              ),
              report,
            ],
          ),
        ),
      );
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('highestBidder', highestBidder))
      ..add(IntProperty('roundNumber', roundNumber));
  }
}
