import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

typedef TeamChangeValue = bool Function();
typedef TeamChange = void Function(bool);

class SingleTeamChooser extends StatefulWidget {
  const SingleTeamChooser(
      {@required this.teamChange,
      @required this.teamOneBid,
      @required this.teams,
      this.labelText,
      Key key})
      : super(key: key);

  final TeamChangeValue teamOneBid;
  final TeamChange teamChange;
  final List<BinokelTeam> teams;

  final String labelText;

  @override
  _SingleTeamChooserState createState() => _SingleTeamChooserState();

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ObjectFlagProperty<TeamChangeValue>.has('teamOneBid', teamOneBid))
      ..add(ObjectFlagProperty<TeamChange>.has('teamChange', teamChange))
      ..add(IterableProperty<BinokelTeam>('teams', teams))
      ..add(StringProperty('labelText', labelText));
  }
}

class _SingleTeamChooserState extends State<SingleTeamChooser> {
  BinokelTeam get teamOne => widget.teams[0];

  BinokelTeam get teamTwo => widget.teams[1];

  bool get teamOneBid => widget.teamOneBid();

  set teamOneBid(bool value) => widget.teamChange(value);

  @override
  Widget build(BuildContext context) {
    final dropdownItems = widget.teams
        .map((team) => DropdownMenuItem(
              value: team,
              child: Text(team.teamName),
            ))
        .toList(growable: false);
    return DropdownButtonFormField(
      decoration: InputDecoration(
          icon: const Icon(Icons.group),
          labelText:
              widget.labelText ?? S.of(context).singleTeamChooserDefaultLabel),
      items: dropdownItems,
      value: teamOneBid ? teamOne : teamTwo,
      onSaved: (team) => teamOneBid = team == teamOne,
      onChanged: (team) {
        setState(() {
          teamOneBid = team == teamOne;
        });
      },
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('teamOne', teamOne))
      ..add(DiagnosticsProperty<BinokelTeam>('teamTwo', teamTwo))
      ..add(DiagnosticsProperty<bool>('teamOneBid', teamOneBid));
  }
}
