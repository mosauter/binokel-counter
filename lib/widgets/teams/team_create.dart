import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CreateTeam extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/team/add';

  final _formKey = GlobalKey<FormState>();
  final _builder = BinokelTeamBuilder();

  @override
  Widget build(BuildContext context) {
    configure(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(S.of(context).createTeamTitle),
        ),
        body: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Form(
            key: _formKey,
            child: Column(children: [
              TextFormField(
                decoration: InputDecoration(
                    icon: const Icon(Icons.group),
                    labelText: S.of(context).createTeamTeamName),
                onSaved: (teamName) => _builder.teamName = teamName.trim(),
                validator: (value) => value.isEmpty
                    ? S.of(context).createTeamEmptyTeamName
                    : null,
              ),
              TextFormField(
                decoration: InputDecoration(
                    icon: const Icon(Icons.person),
                    labelText: S.of(context).createTeamFirstMemberName),
                onSaved: (firstName) =>
                    _builder.teamMemberOne = firstName.trim(),
                validator: (value) => value.isEmpty
                    ? S.of(context).createTeamEmptyFirstMemberName
                    : null,
              ),
              TextFormField(
                decoration: InputDecoration(
                    icon: const Icon(Icons.person),
                    labelText: S.of(context).createTeamSecondMemberName),
                onSaved: (secondName) =>
                    _builder.teamMemberTwo = secondName.trim(),
                validator: (value) => value.isEmpty
                    ? S.of(context).createTeamEmptySecondMemberName
                    : null,
              ),
              RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    bloc.blocSink.add(AddTeam(_builder.build()));
                    Navigator.pop(context);
                  }
                },
                child: Text(S.of(context).submitButton),
              )
            ]),
          ),
        )));
  }
}
