import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TeamListTile extends StatelessWidget {
  const TeamListTile({@required this.team, Key key}) : super(key: key);

  final BinokelTeam team;

  @override
  Widget build(BuildContext context) => ListTile(
        title: Text(team.teamName),
        subtitle: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(S.of(context).teamMemberOne(team.teamMemberOne)),
            Text(S.of(context).teamMemberTwo(team.teamMemberTwo)),
          ],
        ),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<BinokelTeam>('team', team));
  }
}
