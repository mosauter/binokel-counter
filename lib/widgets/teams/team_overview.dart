import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/events.dart';
import 'package:binokel_counter/bloc/game_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:binokel_counter/widgets/teams/team_create.dart';
import 'package:binokel_counter/widgets/teams/team_list_tile.dart';
import 'package:flutter/material.dart';

class TeamOverview extends StatelessWidget with BlocHelper<BinokelBloc> {
  static const String routeName = '/teams';

  @override
  Widget build(BuildContext context) {
    configure(context);

    return Scaffold(
      appBar: AppBar(title: Text(S.of(context).teamOverviewTitle)),
      body: SingleChildScrollView(
          child: StreamBuilder<List<BinokelTeam>>(
        stream: bloc.teamStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            bloc.blocSink.add(TeamRefresh());
            return AppConstants.loadingAnimation;
          }
          final teamList = snapshot.data;

          if (teamList.isEmpty) {
            return AppConstants.emptyMessage(context);
          }

          return ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: teamList.length,
              itemBuilder: (context, index) {
                final team = teamList[index];

                return TeamListTile(team: team);
              });
        },
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pushNamed(context, CreateTeam.routeName),
        child: const Icon(Icons.add),
      ),
    );
  }
}
