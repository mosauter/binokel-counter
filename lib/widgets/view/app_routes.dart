import 'package:binokel_counter/widgets/about/about.dart';
import 'package:binokel_counter/widgets/game/game_create.dart';
import 'package:binokel_counter/widgets/game/game_current.dart';
import 'package:binokel_counter/widgets/main/main_page.dart';
import 'package:binokel_counter/widgets/round/round_create.dart';
import 'package:binokel_counter/widgets/round/round_finish.dart';
import 'package:binokel_counter/widgets/round/round_report.dart';
import 'package:binokel_counter/widgets/teams/team_create.dart';
import 'package:binokel_counter/widgets/teams/team_overview.dart';
import 'package:binokel_counter/widgets/view/changelog.dart';
import 'package:flutter/material.dart';

class BinokelRoutes {
  BinokelRoutes({this.routes, this.initialRoute});

  final Map<String, Widget Function(BuildContext)> routes;
  final String initialRoute;
}

BinokelRoutes getRoutes() => BinokelRoutes(initialRoute: '/', routes: {
      MainPage.routeName: (context) => MainPage(),
      TeamOverview.routeName: (context) => TeamOverview(),
      CreateTeam.routeName: (context) => CreateTeam(),
      CurrentGamePage.routeName: (context) => CurrentGamePage(),
      CreateGamePage.routeName: (context) => CreateGamePage(),
      CreateRoundPage.routeName: (context) => CreateRoundPage(),
      ReportRoundPage.routeName: (context) => ReportRoundPage(),
      FinishRoundPage.routeName: (context) => FinishRoundPage(),
      AboutPage.routeName: (context) => AboutPage(),
      ChangelogPage.routeName: (context) => ChangelogPage(),
    });
