import 'package:binokel_counter/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_whatsnew/flutter_whatsnew.dart';

class ChangelogPage extends StatelessWidget {
  static const String routeName = '/changelog';

  @override
  Widget build(BuildContext context) => WhatsNewPage.changelog(
        title: Text(
          S.of(context).whatsNewTitle,
          textScaleFactor: 1,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.bold,
          ),
        ),
        buttonText: Text(
          S.of(context).continueButton,
          style: const TextStyle(color: Colors.white),
        ),
      );
}
