import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BinokelDefaultScaffold extends StatelessWidget {
  const BinokelDefaultScaffold(
      {@required this.child, @required this.title, Key key, this.actionButton})
      : super(key: key);

  final Widget child;
  final String title;
  final FloatingActionButton actionButton;

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: Text(title)),
      body: child,
      floatingActionButton: actionButton);

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('title', title));
  }
}
