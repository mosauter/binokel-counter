import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/widgets/view/round_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// BinokelGame is the widget for a whole game which consists of multiple
/// rounds, which are represented by the {BinokelRound}-widget.
///
/// This widget should handle the input and ui for the facts that tell
/// something about it, like:
/// * teamnames / playernames
/// * overall score
/// * how many games are played
/// * average game score
/// * probability of winning/loosing a round based on played rounds
class BinokelGameDetailList extends StatelessWidget {
  const BinokelGameDetailList({@required this.game, Key key}) : super(key: key);

  final BinokelGame game;

  @override
  Widget build(BuildContext context) => ListView.builder(
      itemCount: game.rounds.length,
      itemBuilder: (context, index) => BinokelRoundListTile(
            round: game.rounds[index],
            index: index + 1,
            teamOne: game.teamOne,
            teamTwo: game.teamTwo,
          ));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<BinokelGame>('game', game));
  }
}
