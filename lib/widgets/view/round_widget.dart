import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/model/immutable/round/round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class BinokelRoundListTile extends StatelessWidget {
  const BinokelRoundListTile(
      {@required this.round,
      @required this.index,
      @required this.teamOne,
      @required this.teamTwo,
      Key key})
      : super(key: key);

  final BinokelTeam teamOne;
  final BinokelTeam teamTwo;
  final Round round;
  final int index;

  @override
  Widget build(BuildContext context) => ListTile(
        title: Text(S.of(context).roundListTileRoundDescription(
            index,
            round.done
                ? S.of(context).roundListTileDone
                : S.of(context).roundListTileNotDone)),
        subtitle: Text(S.of(context).roundListTileSummary(teamOne.teamName,
            round.resultTeamOne, teamTwo.teamName, round.resultTeamTwo)),
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<BinokelTeam>('teamOne', teamOne))
      ..add(DiagnosticsProperty<BinokelTeam>('teamTwo', teamTwo))
      ..add(DiagnosticsProperty<Round>('round', round))
      ..add(IntProperty('index', index));
  }
}
