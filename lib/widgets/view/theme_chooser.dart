import 'package:binokel_counter/bloc/bloc_helper.dart';
import 'package:binokel_counter/bloc/theme_bloc.dart';
import 'package:binokel_counter/generated/l10n.dart';
import 'package:binokel_counter/widgets/app_constants.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ThemeChooser extends StatelessWidget with BlocHelper<ThemeBloc> {
  @override
  Widget build(BuildContext context) {
    configure(context);
    return StreamBuilder<BinokelThemeMode>(
      stream: bloc.theme,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return AppConstants.loadingAnimation;
        }

        final themeMode = snapshot.data;

        return ListTile(
          leading: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(S.of(context).themeChooser),
            ],
          ),
          title: SizedBox(
            width: 32,
            child: DropdownButtonFormField(
              value: themeMode,
              items: [
                const DropdownMenuItem(
                  value: BinokelThemeMode.system,
                  child: Icon(MdiIcons.brightnessAuto),
                ),
                const DropdownMenuItem(
                  value: BinokelThemeMode.light,
                  child: Icon(MdiIcons.brightness5),
                ),
                const DropdownMenuItem(
                  value: BinokelThemeMode.dark,
                  child: Icon(MdiIcons.brightness7),
                ),
              ],
              onChanged: (mode) => bloc.sink.add(mode),
            ),
          ),
        );
      },
    );
  }
}
