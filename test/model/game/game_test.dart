import 'dart:convert';

import 'package:binokel_counter/model/immutable/game.dart';
import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/model/serializers.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

import '../round/normal_round_test.dart';

BinokelGame exampleGame() => BinokelGame(
      (game) => game
        ..teamOne = (BinokelTeamBuilder()
          ..teamName = 'Team 1'
          ..teamMemberOne = 'Member 1 / 1'
          ..teamMemberTwo = 'Member 2 / 1')
        ..teamTwo = (BinokelTeamBuilder()
          ..teamName = 'Team 2'
          ..teamMemberOne = 'Member 1 / 2'
          ..teamMemberTwo = 'Member 2 / 2'),
    )
        .addNewRound(NormalRound((round) => round
          ..teamOneBid = true
          ..bid = 500
          ..reportedTeamOne = 100
          ..reportedTeamTwo = 100)
          ..setPoints(200, teamOne: true))
        .addNewRound(NormalRound((round) => round
          ..teamOneBid = true
          ..bid = 500
          ..reportedTeamOne = 100
          ..reportedTeamTwo = 100)
          ..setPoints(200, teamOne: true));

const exampleJson = '{'
    '"\$":"BinokelGame",'
    '"teamOne":{"teamName":"Team 1","teamMemberOne":"Member 1 / 1","teamMemberTwo":"Member 2 / 1"},'
    '"teamTwo":{"teamName":"Team 2","teamMemberOne":"Member 1 / 2","teamMemberTwo":"Member 2 / 2"},'
    '"rounds":['
    '{'
    '"\$":"NormalRound",'
    '"bid":500,'
    '"teamOneBid":true,'
    '"reportedTeamOne":100,'
    '"reportedTeamTwo":100'
    '},'
    '{'
    '"\$":"NormalRound",'
    '"bid":500,'
    '"teamOneBid":true,'
    '"reportedTeamOne":100,'
    '"reportedTeamTwo":100'
    '}]}';

@isTest
void main() {
  group('BinokelGame', () {
    test('should serialize to json', () async {
      final game = exampleGame();
      final json = jsonEncode(serializers.serialize(game));

      expect(json, equals(exampleJson));
    });

    test('should deserialize from json', () async {
      final game = deserialize<BinokelGame>(jsonDecode(exampleJson));

      expect(game, equals(exampleGame()));
    });

    test('should create a new instance on addNewRound', () async {
      final gameOne = exampleGame();
      final gameTwo = gameOne.addNewRound(exampleRound());

      expect(identical(gameOne, gameTwo), isFalse);
      expect(gameOne, isNot(equals(gameTwo)));
    });
  });
}
