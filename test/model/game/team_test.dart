import 'dart:convert';

import 'package:binokel_counter/model/immutable/team.dart';
import 'package:binokel_counter/model/serializers.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

BinokelTeam exampleTeam() => BinokelTeam((b) => b
  ..teamName = 'TeamName'
  ..teamMemberOne = 'Member 1'
  ..teamMemberTwo = 'Member 2');

const exampleJson = '{'
    '"\$":"BinokelTeam",'
    '"teamName":"TeamName",'
    '"teamMemberOne":"Member 1",'
    '"teamMemberTwo":"Member 2"'
    '}';

@isTest
void main() {
  const teamName = 'TeamName';
  const memberOne = 'Member 1';
  const memberTwo = 'Member 2';

  group('to json string', () {
    test('should serialize to json', () async {
      final team = exampleTeam();

      final json = jsonEncode(serializers.serialize(team));

      expect(json, equals(exampleJson));
    });

    test('should deserialize from json', () async {
      final team = deserialize<BinokelTeam>(jsonDecode(exampleJson));

      expect(team.teamName, equals(teamName));
      expect(team.teamMemberOne, equals(memberOne));
      expect(team.teamMemberTwo, equals(memberTwo));
    });

    test('two teams should be the same if they match', () async {
      final teamOne = exampleTeam();
      final teamTwo = exampleTeam();

      expect(identical(teamOne, teamTwo), isFalse);
      expect(teamOne, equals(teamTwo));
    });

    test('should equal the toBuilder-build chain', () async {
      final team = BinokelTeam((b) => b
        ..teamName = teamName
        ..teamMemberOne = memberOne
        ..teamMemberTwo = memberTwo);

      expect(team.toBuilder().build(), equals(team));
    });
  });
}
