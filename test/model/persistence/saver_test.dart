import 'dart:io';

import 'package:binokel_counter/model/persistence/saver.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

import '../game/game_test.dart';
import '../game/team_test.dart';

Future<String> get _tmpDirectoryPath async =>
    (await Directory.systemTemp.createTemp()).path;

Future<String> get _teamFilePath async =>
    '${await _tmpDirectoryPath}/binokel_teams.json';

Future<String> get _gameFilePath async =>
    '${await _tmpDirectoryPath}/binokel_games.json';

File _teamFile;
File _gameFile;

bool get _teamFileExists => _teamFile.existsSync();

bool get _gameFileExists => _gameFile.existsSync();

BinokelStorage storage;

@isTest
void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() async {
    _teamFile = File(await _teamFilePath);
    _gameFile = File(await _gameFilePath);

    storage = BinokelStorage(
        gameFile: Future.value(_gameFile), teamFile: Future.value(_teamFile));

    expect(_teamFileExists, isFalse,
        reason: 'File should not exist beforehand');
    expect(_gameFileExists, isFalse,
        reason: 'File should not exist beforehand');
  });

  tearDown(() async {
    if (_teamFileExists) {
      await _teamFile.delete();
    }
    if (_gameFileExists) {
      await _gameFile.delete();
    }
  });

  group('create files', () {
    test('should create "binokel_teams.json"', () async {
      await storage.writeTeams([]);

      expect(_teamFileExists, isTrue);
    });

    test('should create "binokel_games.json"', () async {
      await storage.writeGames([]);

      expect(_gameFileExists, isTrue);
    });
  });

  group('Teams', () {
    test('should load saved teams', () async {
      final teams = [exampleTeam(), exampleTeam()];

      await storage.writeTeams(teams);
      expect(_teamFileExists, isTrue);

      final loadedTeams = await storage.loadTeams();
      expect(loadedTeams, equals(teams));
    });

    test('load should not delete the file', () async {
      final teams = [exampleTeam(), exampleTeam()];

      await storage.writeTeams(teams);
      await storage.loadTeams();

      expect(_teamFileExists, isTrue);
    });

    test('should replace file if new data is coming in', () async {
      final teams = [exampleTeam(), exampleTeam()];

      await storage.writeTeams(teams);
      final loadedTeams = await storage.loadTeams();
      expect(loadedTeams, equals(teams));

      final newTeams = [exampleTeam(), exampleTeam(), exampleTeam()];

      await storage.writeTeams(newTeams);
      final newLoadedTeams = await storage.loadTeams();
      expect(newLoadedTeams, equals(newTeams));
    });
  });

  group('Games', () {
    test('should load saved games', () async {
      final games = [exampleGame(), exampleGame()];

      await storage.writeGames(games);
      expect(_gameFileExists, isTrue);

      final loadedGames = await storage.loadGames();
      expect(loadedGames, equals(games));
    });

    test('load should not delete the file', () async {
      final games = [exampleGame(), exampleGame()];

      await storage.writeGames(games);
      await storage.loadGames();

      expect(_gameFileExists, isTrue);
    });

    test('should replace file if new data is coming in', () async {
      final games = [exampleGame(), exampleGame()];

      await storage.writeGames(games);
      final loadedGames = await storage.loadGames();
      expect(loadedGames, equals(games));

      final newGames = [exampleGame(), exampleGame(), exampleGame()];

      await storage.writeGames(newGames);
      final newLoadedGames = await storage.loadGames();
      expect(newLoadedGames, equals(newGames));
    });
  });

  group('load if file not exists', () {
    test('should return empty games', () async {
      expect(_teamFileExists, isFalse,
          reason: 'File should not exist beforehand');

      final games = await storage.loadGames();

      expect(games, isNotNull);
      expect(games, isEmpty);
    });
    test('should return empty teams', () async {
      expect(_teamFileExists, isFalse,
          reason: 'File should not exist beforehand');

      final teams = await storage.loadTeams();

      expect(teams, isNotNull);
      expect(teams, isEmpty);
    });
  });
}
