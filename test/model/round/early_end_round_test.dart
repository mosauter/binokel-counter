import 'dart:convert';

import 'package:binokel_counter/model/immutable/round/early_end_round.dart';
import 'package:binokel_counter/model/serializers.dart';
import 'package:flutter_test/flutter_test.dart';

EarlyEndRound exampleEarlyEndRound() => EarlyEndRound((round) => round
  ..bid = 200
  ..teamOneBid = true
  ..reported = 100);

const exampleJson =
    '{"\$":"EarlyEndRound","bid":200,"teamOneBid":true,"reported":100}';

void main() {
  group('EarlyEndRound', () {
    test('should always be "done"', () async {
      final round = exampleEarlyEndRound();

      expect(round.done, isTrue);
    });

    test('should serialize to json', () async {
      final round = exampleEarlyEndRound();
      final json = jsonEncode(serializers.serialize(round));

      expect(json, equals(exampleJson));
    });

    test('should deserialize from json', () async {
      final round = deserialize<EarlyEndRound>(jsonDecode(exampleJson));

      expect(round, equals(exampleEarlyEndRound()));
    });
  });

  group('team one', () {
    test('should count correctly', () async {
      const bid = 200;
      const reported = 100;
      final round = EarlyEndRound((round) => round
        ..bid = bid
        ..teamOneBid = true
        ..reported = reported);

      expect(
          round.resultTeamOne, equals(EarlyEndRound.penaltyMultiplier * bid));
      expect(round.resultTeamTwo,
          equals(EarlyEndRound.bonusPointsForOtherTeam + reported));
    });
  });

  group('team two', () {
    test('should count correctly', () async {
      const bid = 200;
      const reported = 100;
      final round = EarlyEndRound((round) => round
        ..bid = bid
        ..teamOneBid = false
        ..reported = reported);

      expect(round.resultTeamOne,
          equals(EarlyEndRound.bonusPointsForOtherTeam + reported));
      expect(
          round.resultTeamTwo, equals(EarlyEndRound.penaltyMultiplier * bid));
    });
  });
}
