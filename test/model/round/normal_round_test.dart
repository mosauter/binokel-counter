import 'dart:convert';

import 'package:binokel_counter/model/immutable/round/normal_round.dart';
import 'package:binokel_counter/model/serializers.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

NormalRound exampleRound() => NormalRound((round) => round
  ..bid = 200
  ..teamOneBid = true);

const exampleJson = '{"\$":"NormalRound","bid":200,"teamOneBid":true}';

@isTest
void main() {
  group('NormalRound', () {
    test('should create a new instance on set points', () {
      final roundOne = exampleRound();
      final roundTwo = roundOne.setPoints(200, teamOne: false);

      expect(identical(roundOne, roundTwo), isFalse);
      expect(roundOne, isNot(equals(roundTwo)));
    });

    test('should serialize to json', () {
      final round = exampleRound();
      final json = jsonEncode(serializers.serialize(round));

      expect(json, equals(exampleJson));
    });

    test('should deserialize from json', () {
      final round = deserialize<NormalRound>(jsonDecode(exampleJson));

      expect(round, equals(exampleRound()));
    });
  });

  group('results', () {
    test('setPoints should set always set the sum of maxPointsPerRound', () {
      final round = NormalRound((b) => b
        ..teamOneBid = true
        ..bid = 250
        ..reportedTeamOne = 100
        ..reportedTeamTwo = 40);
      final roundWithPointsTeamOne = round.setPoints(180, teamOne: true);

      expect(
          roundWithPointsTeamOne.pointsTeamOne +
              roundWithPointsTeamOne.pointsTeamTwo,
          equals(NormalRound.maxPointsPerRound));

      final roundWithPointsTeamTwo = round.setPoints(180, teamOne: false);

      expect(
          roundWithPointsTeamTwo.pointsTeamOne +
              roundWithPointsTeamTwo.pointsTeamTwo,
          equals(NormalRound.maxPointsPerRound));
    });

    test('should return 0 when nothing is reported', () {
      final round = NormalRound((b) => b
        ..teamOneBid = true
        ..bid = 250);

      expect(round.resultTeamOne, equals(0));
      expect(round.resultTeamTwo, equals(0));
    });

    test('should return 0 when no points set', () {
      final round = NormalRound((b) => b
        ..teamOneBid = true
        ..bid = 250
        ..reportedTeamOne = 100
        ..reportedTeamTwo = 40);

      expect(round.resultTeamOne, equals(0));
      expect(round.resultTeamTwo, equals(0));
    });

    group('team one', () {
      test('should count correctly in a good game', () {
        final round = NormalRound((b) => b
          ..teamOneBid = true
          ..bid = 250
          ..reportedTeamOne = 100
          ..reportedTeamTwo = 40);
        final roundWithPoints = round.setPoints(180, teamOne: true);

        expect(roundWithPoints.resultTeamOne, equals(280));
        expect(roundWithPoints.resultTeamTwo, equals(110));
      });

      test(
          'should multiply with missedBidPenaltyMultiplier '
          'when round is not good', () {
        final round = NormalRound((b) => b
          ..teamOneBid = true
          ..bid = 250
          ..reportedTeamOne = 50
          ..reportedTeamTwo = 40);
        final roundWithPoints = round.setPoints(180, teamOne: true);

        expect(roundWithPoints.resultTeamOne,
            equals(NormalRound.missedBidPenaltyMultiplier * 250));
        expect(roundWithPoints.resultTeamTwo, equals(110));
      });

      test('should count correctly in a good game with exact landing', () {
        final round = NormalRound((b) => b
          ..teamOneBid = true
          ..bid = 250
          ..reportedTeamOne = 100
          ..reportedTeamTwo = 40);
        final roundWithPoints = round.setPoints(150, teamOne: true);

        expect(roundWithPoints.resultTeamOne, equals(250));
        expect(roundWithPoints.resultTeamTwo, equals(140));
      });

      test('should return 0 when team made no points', () {
        final round = NormalRound((b) => b
          ..teamOneBid = true
          ..bid = 250
          ..reportedTeamOne = 100
          ..reportedTeamTwo = 200);
        final roundWithPoints = round.setPoints(250, teamOne: true);

        expect(roundWithPoints.resultTeamOne, equals(350));
        expect(roundWithPoints.resultTeamTwo, equals(0));
      });
    });

    group('team two', () {
      test('should count correctly in a good game', () {
        final round = NormalRound((b) => b
          ..teamOneBid = false
          ..bid = 250
          ..reportedTeamOne = 40
          ..reportedTeamTwo = 100);
        final roundWithPoints = round.setPoints(180, teamOne: false);

        expect(roundWithPoints.resultTeamOne, equals(110));
        expect(roundWithPoints.resultTeamTwo, equals(280));
      });

      test(
          'should multiply with missedBidPenaltyMultiplier '
          'when round is not good', () {
        final round = NormalRound((b) => b
          ..teamOneBid = false
          ..bid = 250
          ..reportedTeamOne = 40
          ..reportedTeamTwo = 50);
        final roundWithPoints = round.setPoints(180, teamOne: false);

        expect(roundWithPoints.resultTeamOne, equals(110));
        expect(roundWithPoints.resultTeamTwo,
            equals(NormalRound.missedBidPenaltyMultiplier * 250));
      });

      test('should count correctly in a good game with exact landing', () {
        final round = NormalRound((b) => b
          ..teamOneBid = false
          ..bid = 250
          ..reportedTeamOne = 40
          ..reportedTeamTwo = 100);
        final roundWithPoints = round.setPoints(150, teamOne: false);

        expect(roundWithPoints.resultTeamOne, equals(140));
        expect(roundWithPoints.resultTeamTwo, equals(250));
      });

      test('should return 0 when team made no points', () {
        final round = NormalRound((b) => b
          ..teamOneBid = false
          ..bid = 250
          ..reportedTeamOne = 200
          ..reportedTeamTwo = 100);
        final roundWithPoints = round.setPoints(250, teamOne: false);

        expect(roundWithPoints.resultTeamOne, equals(0));
        expect(roundWithPoints.resultTeamTwo, equals(350));
      });
    });
  });
}
