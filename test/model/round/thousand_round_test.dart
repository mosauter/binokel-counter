import 'dart:convert';

import 'package:binokel_counter/model/immutable/round/thousand_round.dart';
import 'package:binokel_counter/model/immutable/round/thousand_round_result.dart';
import 'package:binokel_counter/model/serializers.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:meta/meta.dart';

ThousandRound exampleThousandRound() => ThousandRound((round) => round
  ..bid = 200
  ..teamOneBid = true);

ThousandRound exampleThousandRoundWithResult() =>
    exampleThousandRound().setResult(ThousandRoundResultEnum.successful);

const exampleJson = '{"\$":"ThousandRound","bid":200,"teamOneBid":true}';
const exampleJsonWithResult = '{'
    '"\$":"ThousandRound",'
    '"bid":200,'
    '"teamOneBid":true,'
    '"roundResult":"successful"'
    '}';

@isTest
void main() {
  group('ThousandRound', () {
    test('should create a new instance on set result', () async {
      final roundOne = exampleThousandRound();
      final roundTwo = roundOne.setResult(ThousandRoundResultEnum.successful);

      expect(identical(roundOne, roundTwo), isFalse);
      expect(roundOne, isNot(equals(roundTwo)));
    });

    test('should serialize to json', () async {
      final round = exampleThousandRound();
      final json = jsonEncode(serializers.serialize(round));

      expect(json, equals(exampleJson));
    });

    test('should serialize to json with a result', () async {
      final round = exampleThousandRoundWithResult();
      final json = jsonEncode(serializers.serialize(round));

      expect(json, equals(exampleJsonWithResult));
    });

    test('should deserialize from json', () async {
      final round = deserialize<ThousandRound>(jsonDecode(exampleJson));

      expect(round, equals(exampleThousandRound()));
    });

    test('should deserialize from json with result', () async {
      final round =
          deserialize<ThousandRound>(jsonDecode(exampleJsonWithResult));

      expect(round, equals(exampleThousandRoundWithResult()));
    });
  });

  group('team one', () {
    test('should count correctly in successful game', () async {
      final round = ThousandRound((round) => round
        ..teamOneBid = true
        ..bid = 200);
      final roundWithResult =
          round.setResult(ThousandRoundResultEnum.successful);

      expect(roundWithResult.resultTeamOne, equals(1000));
      expect(roundWithResult.resultTeamTwo, equals(0));
    });
    test('should count correctly in laid down game', () async {
      final round = ThousandRound((round) => round
        ..teamOneBid = true
        ..bid = 200);
      final roundWithResult = round.setResult(ThousandRoundResultEnum.laidDown);

      expect(roundWithResult.resultTeamOne, equals(2000));
      expect(roundWithResult.resultTeamTwo, equals(0));
    });
    test('should count correctly in failed game', () async {
      final round = ThousandRound((round) => round
        ..teamOneBid = true
        ..bid = 200);
      final roundWithResult = round.setResult(ThousandRoundResultEnum.failed);

      expect(roundWithResult.resultTeamOne, equals(-1000));
      expect(roundWithResult.resultTeamTwo, equals(0));
    });
  });

  group('team two', () {
    test('should count correctly in successful game', () async {
      final round = ThousandRound((round) => round
        ..teamOneBid = false
        ..bid = 200);
      final roundWithResult =
          round.setResult(ThousandRoundResultEnum.successful);

      expect(roundWithResult.resultTeamOne, equals(0));
      expect(roundWithResult.resultTeamTwo, equals(1000));
    });
    test('should count correctly in laid down game', () async {
      final round = ThousandRound((round) => round
        ..teamOneBid = false
        ..bid = 200);
      final roundWithResult = round.setResult(ThousandRoundResultEnum.laidDown);

      expect(roundWithResult.resultTeamOne, equals(0));
      expect(roundWithResult.resultTeamTwo, equals(2000));
    });
    test('should count correctly in failed game', () async {
      final round = ThousandRound((round) => round
        ..teamOneBid = false
        ..bid = 200);
      final roundWithResult = round.setResult(ThousandRoundResultEnum.failed);

      expect(roundWithResult.resultTeamOne, equals(0));
      expect(roundWithResult.resultTeamTwo, equals(-1000));
    });
  });
}
